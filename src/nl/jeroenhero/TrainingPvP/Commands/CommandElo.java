package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.Stats.PlayerStats;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Jeroen on 12:16 29-12-2016.
 */
public class CommandElo implements CommandExecutor {

    public CommandElo() {
        TrainingPvP.getInstance().getCommand("elo").setExecutor(this);
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player))
            return false;
        Player player = (Player) commandSender;
        PlayerData pd = PlayerData.getPlayerData(player);
        if(!Config.mySQL) {
            player.sendMessage(Config.prefix + "ELO Is disabled: No MySQL data setup.");
            return true;
        }
        PlayerStats stats = pd.getStats();
        if(stats == null) {
            player.sendMessage(Config.prefix + LangConfig.STATS_LOADING.getString());
            return true;
        }
        if(stats.getEloMap() == null) {
            player.sendMessage(Config.prefix + ChatColor.RED + "Something went wrong while trying to reach your stats. If this keeps occuring please try to relog or contact an administrator.");
            return true;
        }
        player.sendMessage(Config.prefix + LangConfig.ELO_LIST.getString());
        for(String str : stats.getEloMap().keySet()) {
            player.sendMessage(ChatColor.GREEN + str + ": " + ChatColor.AQUA + stats.getEloMap().get(str));
        }
        return true;
    }
}
