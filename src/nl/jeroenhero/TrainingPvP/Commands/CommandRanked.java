package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Hooks.CombatLogHook;
import nl.jeroenhero.TrainingPvP.Inventories.MatchSelector;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.util.concurrent.TimeUnit;

/**
 * Created by Jeroen on 11:17 15-7-2016.
 */
public class CommandRanked implements CommandExecutor{

    public CommandRanked() {
        TrainingPvP.getInstance().getCommand("ranked").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage("This can only been done by players!");
            return true;
        }
        Player player = (Player) commandSender;
        if(!Config.mySQL) {
            player.sendMessage(Config.prefix + "Ranked Is disabled: No MySQL data setup.");
            return true;
        }
        if(Config.rankedEnabled == false) {
            player.sendMessage(Config.prefix + LangConfig.RANKED_DISABLED.getString());
            return true;
        }
        Permission permission = new Permission("trainingpvp.ranked", PermissionDefault.TRUE);
        if(!player.hasPermission(permission)) {
            player.sendMessage(LangConfig.NO_PERMISSION.getString());
            return true;
        }
        PlayerData pd = PlayerData.getPlayerData(player);
        if(Config.cooldownBetweenRankedEnabled){
            if(System.currentTimeMillis() < pd.getNextRankedMatchMili()) {
                String str = LangConfig.RANKED_COOLDOWN.getString();
                long mili = pd.getNextRankedMatchMili() - System.currentTimeMillis();
                long mins = TimeUnit.MILLISECONDS.toMinutes(mili);
                long sec = TimeUnit.MILLISECONDS.toSeconds(mili);
                String secS = sec + "";
                if(sec < 10)
                    secS = "0" + secS;
                str = str.replace("%time%", mins +  ":" + secS);
                player.sendMessage(Config.prefix + str);
            }
        }
        MatchSelector m = new MatchSelector(player, true, false);
        return true;
    }
}
