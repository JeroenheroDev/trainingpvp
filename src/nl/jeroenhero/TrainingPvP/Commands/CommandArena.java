package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Inventories.ArenaGUI;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

/**
 * Created by Jeroen on 19:55 19-7-2016.
 */
public class CommandArena implements CommandExecutor {

    public CommandArena() {
        TrainingPvP.getInstance().getCommand("arena").setExecutor(this);
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage("Players only!");
            return true;
        }
        Player player = (Player) commandSender;
        Permission perm = new Permission("trainingpvp.arena", PermissionDefault.OP);
        if(player.hasPermission(perm)) {
            new ArenaGUI(player).open();
        }
        else {
            player.sendMessage(Config.prefix + LangConfig.NO_PERMISSION.getString());
        }
        return true;
    }
}
