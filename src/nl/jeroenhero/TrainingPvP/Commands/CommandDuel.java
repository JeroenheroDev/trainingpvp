package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Hooks.CombatLogHook;
import nl.jeroenhero.TrainingPvP.Inventories.KitSelector;
import nl.jeroenhero.TrainingPvP.Modes.Mode1v1;
import nl.jeroenhero.TrainingPvP.Objects.*;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.util.ArrayList;

/**
 * Created by Jeroen on 11:17 15-7-2016.
 */
public class CommandDuel extends BukkitCommand{

    public CommandDuel(String name) {
        super(name);
        this.description = "Command for dueling other players.";
        this.usageMessage = "/" + name;
        this.setAliases(new ArrayList<String>());
    }
    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        final Player player = (Player) commandSender;
        Permission perm = new Permission("trainingpvp.duel", PermissionDefault.TRUE);
        if(!player.hasPermission(perm)) {
            player.sendMessage(LangConfig.NO_PERMISSION.getString());
            return true;
        }
        if(CombatLogHook.isTagged(player)) {
            player.sendMessage(Config.prefix + LangConfig.COMBAT_TAGGED.getString());
            return true;
        }
        PlayerData playerData = PlayerData.getPlayerData(player);
        if(playerData.getCurrentMatch() != null) {
            player.sendMessage(Config.prefix + LangConfig.ALREADY_IN_A_GAME.getString());
            return true;
        }
        if(playerData.isInQueue()) {
            player.sendMessage(Config.prefix + LangConfig.IN_A_QUEUE.getString());
            return true;
        }
        if(args.length < 1) {
            sendHelpMessage(player);
            return true;
        }
        if(args[0].equals("accept")) {
            if(args.length < 2) {
                player.sendMessage(Config.prefix + "/" + Config.duelCommand + " accept <Player>");
                return true;
            }
            if(Bukkit.getPlayer(args[1]) == null) {
                player.sendMessage(Config.prefix + LangConfig.UNKNOWN_PLAYER.getString());
                return true;
            }
            final Player target = Bukkit.getPlayer(args[1]);
            PlayerData pd = PlayerData.getPlayerData(target);
            if(!pd.isInvitedForDuel(player.getUniqueId())) {
                player.sendMessage(Config.prefix + LangConfig.NOT_INVITED.getString());
                return true;
            }
            if(pd.getCurrentMatch() != null) {
                player.sendMessage(Config.prefix + LangConfig.OTHER_ALREADY_IN_A_GAME.getString());
                return true;
            }
            if(BattleQueue.isInQueue(target)) {
                player.sendMessage(Config.prefix + LangConfig.OTHER_ALREADY_WAITING_FOR_GAME.getString());
                return true;
            }
            DuelInvite invite = pd.getInvite(player.getUniqueId());
            pd.removeInvite(player.getUniqueId());
            Match m = new Match(new Mode1v1());
            ArrayList<Player> aPlayers = new ArrayList<Player>() {{
                add(player);
            }};
            final Team teamA = new Team(aPlayers);
            ArrayList<Player> bPlayers = new ArrayList<Player>() {{
                add(target);
            }};
            final Team teamB = new Team(bPlayers);
            ArrayList<Team> teamList = new ArrayList<Team>() {{
                add(teamA);
                add(teamB);
            }};
            m.start(new Mode1v1(), teamList, invite.kit);
        }
        else {
            if(Bukkit.getPlayer(args[0]) == null) {
                player.sendMessage(Config.prefix + LangConfig.UNKNOWN_PLAYER.getString());
                return true;
            }
            final Player target = Bukkit.getPlayer(args[0]);
            if(target == player) {
                player.sendMessage(Config.prefix + LangConfig.CANT_DUEL_YOURSELF.getString());
                return true;
            }
            PlayerData pd = PlayerData.getPlayerData(target);
            if(pd.getCurrentMatch() != null) {
                player.sendMessage(Config.prefix + LangConfig.OTHER_ALREADY_IN_A_GAME.getString(target));
                return true;
            }
            if(BattleQueue.isInQueue(target)) {
                player.sendMessage(Config.prefix + LangConfig.OTHER_ALREADY_WAITING_FOR_GAME.getString(target));
                return true;
            }
            playerData.setDuelInviting(target);
            new KitSelector(player, new Mode1v1(), false, true);
        }
        return true;
    }

    private void sendHelpMessage(Player player) {
        player.sendMessage(Config.prefix + "Help for /" + Config.duelCommand);
        player.sendMessage(Config.prefix + "/" + Config.duelCommand + " <Player> - Invites a player for a duel");
        player.sendMessage(Config.prefix + "/" + Config.duelCommand + " accept <Player> - Accept a duel invite from a a player.");
    }
}
