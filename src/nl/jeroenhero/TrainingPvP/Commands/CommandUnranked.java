package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Hooks.CombatLogHook;
import nl.jeroenhero.TrainingPvP.Inventories.MatchSelector;
import nl.jeroenhero.TrainingPvP.Objects.BattleMode;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

/**
 * Created by Jeroen on 11:17 15-7-2016.
 */
public class CommandUnranked implements CommandExecutor{

    public CommandUnranked() {
        TrainingPvP.getInstance().getCommand("unranked").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage("This can only been done by players!");
            return true;
        }
        Player player = (Player) commandSender;
        Permission permission = new Permission("trainingpvp.unranked", PermissionDefault.TRUE);
        if(!player.hasPermission(permission)) {
            player.sendMessage(LangConfig.NO_PERMISSION.getString());
            return true;
        }
        MatchSelector m = new MatchSelector(player, false, false);
        return true;
    }
}
