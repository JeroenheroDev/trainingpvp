package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Hooks.CombatLogHook;
import nl.jeroenhero.TrainingPvP.Kits.ConfigKit;
import nl.jeroenhero.TrainingPvP.Kits.EditableKitManager;
import nl.jeroenhero.TrainingPvP.Kits.Kit;
import nl.jeroenhero.TrainingPvP.Kits.KitManager;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jeroen on 12:42 29-12-2016.
 */
public class CommandEdit extends BukkitCommand implements Listener {

    public CommandEdit(String name) {
        super(name);
        this.description = "Command for editing your kits!";
        this.usageMessage = "/" + name;
        this.setAliases(new ArrayList<String>());
        Bukkit.getPluginManager().registerEvents(this, TrainingPvP.getInstance());
    }

    private HashMap<ItemStack, Kit> kitMap = new HashMap<ItemStack, Kit>();

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        if(!(commandSender instanceof Player))
            return false;
        Player player = (Player) commandSender;
        Permission editPerm = new Permission("trainingpvp.edit", PermissionDefault.TRUE);
        if(!player.hasPermission(editPerm)) {
            player.sendMessage(Config.prefix + LangConfig.NO_PERMISSION.getString());
            return true;
        }
        if(!editCheck(player))
            return true;
        Inventory inv = Bukkit.createInventory(null, (1 + (int)(KitManager.kits.size() / 9.0)) * 9, LangConfig.EDIT_GUI_NAME.getString());
        int i = 0;
        for(Kit k : KitManager.kits) {
            if(!(k instanceof ConfigKit))
                continue;
            ItemStack editItem = k.getGuiItem();
            ItemMeta itm = editItem.getItemMeta();
            List<String> lore = itm.getLore();
            lore.add(ChatColor.GREEN + LangConfig.EDIT_KIT_LORE.getString());
            itm.setLore(lore);
            editItem.setItemMeta(itm);
            kitMap.put(editItem, k);
            inv.setItem(i, editItem);
            i++;
        }
        player.openInventory(inv);
        return true;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if(event.getInventory().getName().equalsIgnoreCase(LangConfig.EDIT_GUI_NAME.getString())) {
            if(event.getCurrentItem() == null)
                return;
            if(!editCheck((Player)event.getWhoClicked())) {
                event.getWhoClicked().closeInventory();
                return;
            }
            if(kitMap.containsKey(event.getCurrentItem())) {
                event.getWhoClicked().closeInventory();
                Kit k = kitMap.get(event.getCurrentItem());
                ServiceLocator.getEditableKitManager().editKit((Player)event.getWhoClicked(), (ConfigKit)k);
            }
        }
    }

    private boolean editCheck(Player player) {
        PlayerData pd = PlayerData.getPlayerData(player);
        if(pd.isInQueue()) {
            player.sendMessage(Config.prefix + LangConfig.ALREADY_IN_QUEUE.getString());
            return false;
        }
        if(pd.getCurrentMatch() != null) {
            player.sendMessage(Config.prefix + LangConfig.ALREADY_IN_A_GAME.getString());
            return false;
        }
        if(CombatLogHook.isTagged(player)) {
            player.sendMessage(Config.prefix + LangConfig.COMBAT_TAGGED.getString());
            return false;
        }
        return true;
    }
}
