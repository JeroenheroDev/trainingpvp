package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.Objects.Party;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Jeroen on 21:42 19-7-2016.
 */
public class CommandParty extends BukkitCommand {

    public CommandParty(String name) {
        super(name);
        this.description = "Create a party to play with friends!";
        this.usageMessage = "/party";
        this.setAliases(new ArrayList<String>());
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            PlayerData playerData = PlayerData.getPlayerData(player);
            if(args.length == 0) {
                sendHelpMessage(player);
                return true;
            }
            if(args[0].equalsIgnoreCase("create")) {
                if(playerData.getParty() != null) {
                    player.sendMessage(Config.prefix + LangConfig.ALREADY_IN_PARTY.getString());
                    return true;
                }
                Party party = new Party();
                party.setLeader(player);
                party.addMember(player);
                player.sendMessage(Config.prefix + LangConfig.PARTY_CREATED.getString());
                return true;
            }
            else if(args[0].equalsIgnoreCase("kick")) {
                Party party = playerData.getParty();
                if(party == null) {
                    player.sendMessage(Config.prefix + LangConfig.NOT_IN_PARTY.getString());
                    return true;
                }
                else {
                    if(party.getLeader() != player) {
                        player.sendMessage(Config.prefix + LangConfig.NOT_PARTY_LEADER.getString());
                        return true;
                    }
                    if(args.length < 2) {
                        player.sendMessage(Config.prefix + "/" + Config.partyCommand + " kick <Name>");
                        return true;
                    }
                    if(Bukkit.getPlayer(args[1]) == null) {
                        player.sendMessage(Config.prefix + LangConfig.UNKNOWN_PLAYER.getString());
                        return true;
                    }
                    else {
                        Player target = Bukkit.getPlayer(args[1]);
                        if(party.getMembers().contains(target)) {
                            if(target == player) {
                                player.sendMessage(Config.prefix + LangConfig.CANT_SELF.getString());
                            }
                            party.kickPlayer(target);
                        }
                        else {
                            player.sendMessage(Config.prefix + LangConfig.PLAYER_NOT_IN_YOUR_PARTY.getString());
                        }
                    }
                    return true;
                }
            }
            else if(args[0].equalsIgnoreCase("disband") || args[0].equalsIgnoreCase("remove")) {
                Party party = playerData.getParty();
                if(party == null) {
                    player.sendMessage(Config.prefix + LangConfig.NOT_IN_PARTY.getString());
                    return true;
                }
                else {
                    if(party.getLeader() != player) {
                        player.sendMessage(Config.prefix + LangConfig.NOT_PARTY_LEADER.getString());
                        return true;
                    }
                    party.disband();
                    return true;
                }
            }
            else if(args[0].equalsIgnoreCase("invite")) {
                Party party = playerData.getParty();
                if(party == null) {
                    player.sendMessage(Config.prefix + LangConfig.NOT_IN_PARTY.getString());
                    return true;
                }
                if(party.getLeader() != player) {
                    player.sendMessage(Config.prefix + LangConfig.NOT_PARTY_LEADER.getString());
                    return true;
                }
                if(args.length < 2) {
                    player.sendMessage(Config.prefix + "/" + Config.partyCommand + " invite <Player>");
                    return true;
                }
                String targetName = args[1];
                if(Bukkit.getPlayer(targetName) == null) {
                    player.sendMessage(Config.prefix + LangConfig.UNKNOWN_PLAYER.getString());
                    return true;
                }
                Player target = Bukkit.getPlayer(args[1]);
                if(party.isInvited(target.getUniqueId())) {
                    player.sendMessage(Config.prefix + LangConfig.ALREADY_INVITED.getString());
                    return true;
                }
                if(PlayerData.getPlayerData(target).getParty() != null) {
                    player.sendMessage(Config.prefix + LangConfig.OTHER_ALREADY_IN_PARTY.getString());
                    return true;
                }
                party.addInvite(target.getUniqueId());
                target.sendMessage(Config.prefix + LangConfig.PARTY_INVITED_MSG.getString(player));
                String clickToAcceptMsg = "tellraw " + target.getName() + " [\"\",{\"text\":\"%MESSAGE%\",\"color\":\"green\",\"bold\":true,\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/party accept %INVITER%\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"/party accept %INVITER%\",\"color\":\"yellow\"}]}}}]";
                clickToAcceptMsg = clickToAcceptMsg.replace("%INVITER%", player.getName());
                clickToAcceptMsg = clickToAcceptMsg.replace("%MESSAGE%", LangConfig.CLICK_TO_JOIN_PARTY.getString());
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), clickToAcceptMsg);
                party.sendAllMessage(LangConfig.PARTY_INVITED_OTHER.getString(target));
            }
            else if(args[0].equalsIgnoreCase("accept")) {
                if(args.length < 2) {
                    player.sendMessage(Config.prefix + "/" + Config.partyCommand + " accept <Name>");
                    return true;
                }
                else {
                    if(Bukkit.getPlayer(args[1]) == null) {
                        player.sendMessage(Config.prefix + LangConfig.UNKNOWN_PLAYER.getString());
                        return true;
                    }
                    else {
                        Player target = Bukkit.getPlayer(args[1]);
                        if(PlayerData.getPlayerData(target).getParty() != null) {
                            Party party = PlayerData.getPlayerData(target).getParty();
                            if(party.isInvited(player.getUniqueId())) {
                                party.addMember(player);
                                party.unInvite(player.getUniqueId());
                            }
                            else {
                                player.sendMessage(Config.prefix + LangConfig.NOT_INVITED_FOR_PARTY.getString());
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(Config.prefix + LangConfig.PLAYER_NOT_IN_PARTY.getString());
                            return true;
                        }
                    }
                }
            }
            else if(args[0].equalsIgnoreCase("list")) {
                if(Party.parties.size() < 1) {
                    player.sendMessage(Config.prefix + LangConfig.NO_PARTIES_AVAILABLE.getString());
                    return true;
                }
                player.sendMessage(Config.prefix + LangConfig.PARTY_LIST.getString());
                for(Party party : Party.parties) {
                    String partyMessage = "tellraw " + player.getName() + " [\"\",{\"text\":\"- %LEADER%(\",\"color\":\"green\"},{\"text\":\"%AMOUNT%\",\"color\":\"yellow\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"%LEADER%\n\",\"color\":\"green\"}";
                    partyMessage = partyMessage.replace("%LEADER%", party.getLeader().getName());
                    partyMessage = partyMessage.replace("%AMOUNT%", party.getMembers().size() + "");
                    for(Player target : party.getMembers()) {
                        if(target != party.getLeader()) {
                            partyMessage = partyMessage + ",{\"text\":\"%MEMBER%\n\",\"color\":\"yellow\"}";
                            partyMessage = partyMessage.replace("%MEMBER%", target.getName());
                        }
                    }
                    partyMessage = partyMessage + "]}}},{\"text\":\")\",\"color\":\"green\"}]";
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), partyMessage);

                    //player.sendMessage(ChatColor.YELLOW + "- " + ChatColor.GREEN + party.getLeader().getName() + " (" + ChatColor.YELLOW + party.getMembers().size() + ChatColor.GREEN + ")");
                }
            }
            else if(args[0].equalsIgnoreCase("leave")) {
                Party party = playerData.getParty();
                if(party == null) {
                    player.sendMessage(Config.prefix + LangConfig.NOT_IN_PARTY.getString());
                    return true;
                }
                party.removePlayer(player);
                return true;
            }
            else {
                sendHelpMessage(player);
            }

        }
        return true;
    }

    private void sendHelpMessage(Player player) {
        player.sendMessage(Config.prefix + "Help for /" + Config.partyCommand);
        String prefix = Config.prefix + ChatColor.GREEN + "/" + Config.partyCommand + " ";
        ChatColor color = ChatColor.AQUA;
        player.sendMessage(prefix + "create " + color + "- Create a party!");
        player.sendMessage(prefix + "invite <Player> " + color + "- Invite a player for your party!");
        player.sendMessage(prefix + "kick <Player> " + color + "- Kick a player from your party!");
        player.sendMessage(prefix + "accept <Player> " + color + "- Accept a party invite from a player!");
        player.sendMessage(prefix + "list " + color + "- Get a list of all parties!");
        player.sendMessage(prefix + "disband || remove" + color + "- Remove your party!");

    }
}
