package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Kits.ConfigKit;
import nl.jeroenhero.TrainingPvP.Kits.Kit;
import nl.jeroenhero.TrainingPvP.Kits.KitManager;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Jeroen on 15:17 24-8-2016.
 */
public class CommandKit extends BukkitCommand {

    public CommandKit(String name) {
        super(name);
        this.description = "Command for setting up and managing kits.";
        this.usageMessage = "/kit";
        this.setAliases(new ArrayList<String>());
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage("Players only");
            return true;
        }
        Player player = (Player) commandSender;
        Permission perm = new Permission("trainingpvp.kit", PermissionDefault.OP);
        if(!player.hasPermission(perm)) {
            player.sendMessage(Config.prefix + LangConfig.NO_PERMISSION.getString());
            return true;
        }
        if(args.length == 0) {
            sendHelpMessage(player);
            return true;
        }
        if(args[0].equalsIgnoreCase("create")) {
            if(args.length == 2) {
                ServiceLocator.getKitManager().createConfigKit(args[1], player);
            }
            else {
                player.sendMessage(Config.prefix + "/" + Config.kitCommand + " create <Name>");
            }
        }
        else if(args[0].equalsIgnoreCase("remove")){
            if(args.length == 2) {
                ServiceLocator.getKitManager().removeKit(args[1], player);
            }
            else {
                player.sendMessage(Config.prefix + "/" + Config.kitCommand + " remove <Name>");
            }
        }
        else if(args[0].equalsIgnoreCase("list")) {
            player.sendMessage(Config.prefix + "A list with all loaded kits:");
            for(Kit k : KitManager.kits) {
                player.sendMessage(ChatColor.YELLOW + "- " + ChatColor.GREEN + k.getName());
            }
        }
        else if(args[0].equalsIgnoreCase("show")) {
            if(args.length == 2) {
                Kit kit = null;
                for(Kit k : KitManager.kits) {
                    if(k.getName().equalsIgnoreCase(args[1])) {
                        kit = k;
                    }
                }
                if(kit != null) {
                    kit.giveItems(player);
                }
                else
                    player.sendMessage(Config.prefix + "No kit found under the name " + args[1] + ". Try /kit list for a list of kits.");
            }
            else {
                player.sendMessage(Config.prefix + "/" + Config.kitCommand + " show <Name>");
            }
        }
        else if(args[0].equalsIgnoreCase("setranked")) {
            if(args.length < 3) {
                player.sendMessage(Config.prefix + "/kit setranked <Name> <True/False>");
                return true;
            }
            String kitName = args[2];
            File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Kits/", kitName + ".yml");
            if(!f.exists()) {
                player.sendMessage(Config.prefix + "This kit doesn't exists! For a list of kits, type /kit list.");
                return true;
            }


        }
        else {
            sendHelpMessage(player);
        }
        return true;
    }

    private void sendHelpMessage(Player player) {
        player.sendMessage(Config.prefix + "Help for /kit");
        player.sendMessage(Config.prefix + "/kit create <Name> - Create a new kit with given name and your current inventory!");
        player.sendMessage(Config.prefix + "/kit remove <Name> - Remove a kit");
        player.sendMessage(Config.prefix + "/kit list - Gives a list of all kits" );
        player.sendMessage(Config.prefix + "/kit show <Name> - Shows you the kit!");
        player.sendMessage(Config.prefix + "/kit setranked <Name> <True/False> - Set if a kit is ranked or not.");
    }
}
