package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Hooks.CombatLogHook;
import nl.jeroenhero.TrainingPvP.Inventories.MatchSelector;
import nl.jeroenhero.TrainingPvP.Objects.BattleMode;
import nl.jeroenhero.TrainingPvP.Objects.BattleQueue;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import nl.jeroenhero.TrainingPvP.Utils.ExecutableItemstack;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

/**
 * Created by Jeroen on 11:17 15-7-2016.
 */
public class CommandBattle implements CommandExecutor{

    public CommandBattle() {
        TrainingPvP.getInstance().getCommand("battle").setExecutor(this);
        s = s + "";
    }

    String s = "%%__USER__%%";

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String str, String[] strings) {
        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage("This can only been done by players!");
            return true;
        }
        Player player = (Player) commandSender;
        Permission permission = new Permission("trainingpvp.battle", PermissionDefault.TRUE);
        if(!player.hasPermission(permission)) {
            player.sendMessage(LangConfig.NO_PERMISSION.getString());
            return true;
        }
        if(strings.length > 0) {
            if(strings[0].equalsIgnoreCase("leave")) {
                BattleQueue.removeFromQueue(player);
                return true;
            }
            if(strings[0].equalsIgnoreCase("id") ) {
                player.sendMessage(s);
                return true;
            }
        }
        MatchSelector m = new MatchSelector(player, false, false);

        return true;
    }
}
