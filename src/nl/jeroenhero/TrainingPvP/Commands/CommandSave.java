package nl.jeroenhero.TrainingPvP.Commands;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Jeroen on 18:37 4-1-2017.
 */
public class CommandSave extends BukkitCommand{

    public CommandSave(String s) {
        super(s);
        this.description = "Command for saving your edited kit!";
        this.usageMessage = "/" + s;
        this.setAliases(new ArrayList<String>());
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        boolean b = ServiceLocator.getEditableKitManager().stopAndSave((Player)commandSender);
        if(!b) {
            commandSender.sendMessage(Config.prefix + LangConfig.CANNOT_SAVE.getString());
        }
        return true;
    }
}
