package nl.jeroenhero.TrainingPvP;

import nl.jeroenhero.TrainingPvP.Arenas.ArenaManager;
import nl.jeroenhero.TrainingPvP.Kits.EditableKitManager;
import nl.jeroenhero.TrainingPvP.Kits.KitManager;
import nl.jeroenhero.TrainingPvP.Objects.BattleQueue;
import nl.jeroenhero.TrainingPvP.SpawnItems.ItemManager;
import nl.jeroenhero.TrainingPvP.Stats.StatsDatabase;
import nl.jeroenhero.TrainingPvP.Utils.InventorySaver;

/**
 * Created by Jeroen on 11:28 15-7-2016.
 */
public class ServiceLocator {

    private static ArenaManager arenaManager;
    private static KitManager kitManager;
    private static BattleQueue battleQueue;
    private static InventorySaver inventorySaver;
    private static EditableKitManager editableKitManager;
    private static StatsDatabase statsDatabase;
    private static ItemManager itemManager;

    public static StatsDatabase getStatsDatabase() {
        if(statsDatabase == null)
            statsDatabase = new StatsDatabase();
        return statsDatabase;
    }

    public static InventorySaver getInventorySaver() {
        if(inventorySaver == null)
            inventorySaver = new InventorySaver();
        return inventorySaver;
    }

    public static EditableKitManager getEditableKitManager() {
        if(editableKitManager == null)
            editableKitManager = new EditableKitManager();
        return editableKitManager;
    }

    public static ArenaManager getArenaManager() {
        return arenaManager;
    }

    public static void setArenaManager(ArenaManager arenaManager) {
        ServiceLocator.arenaManager = arenaManager;
    }


    public static KitManager getKitManager() {
        return kitManager;
    }

    public static void setKitManager(KitManager kitManager) {
        ServiceLocator.kitManager = kitManager;
    }

    public static BattleQueue getBattleQueue() {
        if(battleQueue != null) {
            battleQueue = new BattleQueue();
        }
        return battleQueue;
    }

    public static ItemManager getItemManager() {
        if(itemManager == null)
            return new ItemManager();
        return itemManager;
    }

    public static void setItemManager(ItemManager itemManager) {
        ServiceLocator.itemManager = itemManager;
    }
}
