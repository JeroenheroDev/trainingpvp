package nl.jeroenhero.TrainingPvP.Inventories;

import nl.jeroenhero.TrainingPvP.Hooks.CombatLogHook;
import nl.jeroenhero.TrainingPvP.Modes.Mode1v1;
import nl.jeroenhero.TrainingPvP.Objects.*;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jeroen on 17:05 29-8-2016.
 */
public class MatchSelector implements Listener{

    private HashMap<ItemStack, BattleMode> itemMap = new HashMap<ItemStack, BattleMode>();

    private Player player;

    public MatchSelector(Player player) {
        Bukkit.getPluginManager().registerEvents(this, TrainingPvP.getInstance());
        Inventory inv = Bukkit.createInventory(null, 9, LangConfig.MODE_SELECTOR_GUI_TITLE.getString());
        ArrayList<BattleMode> modes = new ArrayList<BattleMode>();
        for(BattleMode mode : BattleMode.modes) {
            if(mode.isEnabled() == false)
                continue;
            modes.add(mode);
        }
        int i = 0;
        for(BattleMode mode : modes) {
            ItemStack guiItem = mode.getGUIItem().clone();
            List<String> lore = guiItem.getItemMeta().getLore();
            for(String s : lore) {
                if(s.contains("%GAMES%")) {
                    int matchAmount = 0;
                    for(Match m : Match.matches) {
                        if(m.getMode() == mode) {
                            matchAmount ++;
                        }
                    }
                    s = s.replace("%GAMES%", matchAmount + "");
                }
                if(s.contains("%QUEUE%")) {
                    int amount = 0;
                    for(BattleQueue.Queue q : BattleQueue.queueList) {
                        if(q.getMode() == mode.getName()) {
                            amount ++;
                        }
                    }
                    s = s.replace("%QUEUE%", amount + "");
                }
            }
            ItemMeta itm = guiItem.getItemMeta();
            itm.setLore(lore);
            guiItem.setItemMeta(itm);
            inv.setItem(i, guiItem);
            itemMap.put(mode.getGUIItem().clone(), mode);
            i ++;
        }
        player.openInventory(inv);
        this.player = player;
    }

    public MatchSelector(Player player, boolean ranked, boolean duel) {
        if(BattleQueue.isInQueue(player)) {
            player.sendMessage(Config.prefix + LangConfig.ALREADY_IN_QUEUE.getString());
            return;
        }
        PlayerData pd = PlayerData.getPlayerData(player);
        if(pd.getCurrentMatch() != null) {
            player.sendMessage(Config.prefix + LangConfig.ALREADY_IN_A_GAME.getString());
            return;
        }
        if(CombatLogHook.isTagged(player)) {
            player.sendMessage(Config.prefix + LangConfig.COMBAT_TAGGED.getString());
            return;
        }
        if(pd.isEditingKit()) {
            player.sendMessage(Config.prefix + LangConfig.ALREADY_EDITING_KITS.getString());
            return;
        }
        if(duel || ranked) {
            player.closeInventory();
            new KitSelector(player, new Mode1v1(), ranked, duel);
            return;
        }
        new MatchSelector(player);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if(e.getInventory().getName().equals(LangConfig.MODE_SELECTOR_GUI_TITLE.getString()) && e.getWhoClicked() == player) {
            e.setCancelled(true);
            if(itemMap.containsKey(e.getCurrentItem())) {
                e.getWhoClicked().closeInventory();
                BattleMode mode = itemMap.get(e.getCurrentItem());
                new KitSelector((Player)e.getWhoClicked(), mode, false, false);
                HandlerList.unregisterAll(this);
            }
        }
    }


}
