package nl.jeroenhero.TrainingPvP.Inventories;

import nl.jeroenhero.TrainingPvP.Enums.RankedType;
import nl.jeroenhero.TrainingPvP.Kits.Kit;
import nl.jeroenhero.TrainingPvP.Kits.KitManager;
import nl.jeroenhero.TrainingPvP.Objects.*;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jeroen on 08:52 30-8-2016.
 */
public class KitSelector implements Listener{

    private HashMap<ItemStack, Kit> itemMap = new HashMap<ItemStack, Kit>();

    private boolean duel;
    private boolean ranked;
    private Player player;
    private BattleMode mode;

    public KitSelector(Player player, BattleMode mode, boolean ranked, boolean duel) {
        Bukkit.getPluginManager().registerEvents(this, TrainingPvP.getInstance());
        this.duel = duel;
        this.ranked = ranked;
        this.player = player;
        this.mode = mode;
        ArrayList<Kit> kitList = new ArrayList<Kit>();
        for(Kit kit : KitManager.kits) {
            if(kit.getRankType() == RankedType.RANKED && ranked) {
                kitList.add(kit);
            }
            else if(kit.getRankType() == RankedType.UNRANKED && !ranked)
                kitList.add(kit);
        }
        double kitSize = ((double)kitList.size() / 9.0) + 1;
        Inventory inv = Bukkit.createInventory(null, (int) kitSize * 9, LangConfig.KIT_SELECTOR_GUI_TITLE.getString());
        int i = 0;
        for(Kit k : kitList) {
            ItemStack guiItem = k.getGuiItem().clone();
            List<String> lore = guiItem.getItemMeta().getLore();
            for(String s : lore) {
                if(s.contains("%GAMES%")) {
                    int matchAmount = 0;
                    for(Match m : Match.matches) {
                        if(m.getMode() == mode && m.getKit() == k) {
                            matchAmount ++;
                        }
                    }
                    s = s.replace("%GAMES%", matchAmount + "");
                }
                if(s.contains("%QUEUE%")) {
                    int amount = 0;
                    for(BattleQueue.Queue q : BattleQueue.queueList) {
                        if(q.getMode() == mode.getName() && q.getKit() == k.getName()) {
                            amount ++;
                        }
                    }
                    s = s.replace("%QUEUE%", amount + "");
                }
            }
            ItemMeta itm = guiItem.getItemMeta();
            itm.setLore(lore);
            guiItem.setItemMeta(itm);
            inv.setItem(i, guiItem);
            itemMap.put(guiItem, k);
            i++;
        }
        player.openInventory(inv);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if(e.getInventory().getName().equals(LangConfig.KIT_SELECTOR_GUI_TITLE.getString()) && e.getWhoClicked() == player) {
            e.setCancelled(true);
            if(itemMap.containsKey(e.getCurrentItem())) {
                e.getWhoClicked().closeInventory();
                Kit kit = itemMap.get(e.getCurrentItem());
                PlayerData pd = PlayerData.getPlayerData(player);
                if(duel) {
                    Player invited = pd.getDuelInviting();
                    if(invited == null || !invited.isOnline()){
                        player.sendMessage(Config.prefix + LangConfig.DUEL_OPPONENT_OFFLINE.getString());
                        return;
                    }
                    DuelInvite duelInvite = new DuelInvite(player, pd.getDuelInviting(), kit);
                    pd.setDuelInviting(null);
                    pd.inviteForDuel(invited,duelInvite);
                    invited.sendMessage(Config.prefix + LangConfig.DUEL_CHALLENGE.getString(player));
                    player.sendMessage(Config.prefix + LangConfig.DUEL_INVITE_SEND.getString());

                    String clickToAcceptMsg = "tellraw " + invited.getName() + "  [\"\",{\"text\":\"%MESSAGE%\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/duel accept %INVITER%\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"/duel accept %INVITER%\",\"color\":\"green\"}]}}}]";
                    clickToAcceptMsg = clickToAcceptMsg.replace("%MESSAGE%", LangConfig.CLICK_TO_ACCEPT_DUEL.getString());
                    clickToAcceptMsg = clickToAcceptMsg.replace("%INVITER%", player.getName());
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), clickToAcceptMsg);
                    player.closeInventory();
                    return;
                }
                ArrayList<Player> players = new ArrayList<Player>();
                if(pd.getParty() != null) {
                    Party party = pd.getParty();
                    for(Player player : party.getMembers()) {
                        players.add(player);
                    }
                }
                else {
                    players.add(player);
                }
                if(players.size() > mode.getTeamSize()) {
                    player.sendMessage(Config.prefix + LangConfig.PARTY_TOO_BIG.getString());
                    player.closeInventory();
                    return;
                }
                if(players.size() < mode.getTeamSize()) {
                    player.sendMessage(Config.prefix + LangConfig.PARTY_TOO_SMALL.getString());
                    player.closeInventory();
                    return;
                }
                final Team team = new Team(players);
                final Team target = BattleQueue.getQueue(mode, kit.getName(), ranked);
                if(target == null) {
                    BattleQueue.addToQueue(mode, team, kit, ranked);
                    for(Player player : team.players) {
                        player.sendMessage(Config.prefix + LangConfig.JOINED_QUEUE.getString());
                    }
                }
                else {
                    Match m = new Match(mode);
                    m.start(mode, new ArrayList<Team>() {{
                        add(team);
                        add(target);
                    }}, kit);
                    m.setRanked(ranked);
                    BattleQueue.resetQueue(team);
                    BattleQueue.resetQueue(target);
                }

            }
        }
    }
}
