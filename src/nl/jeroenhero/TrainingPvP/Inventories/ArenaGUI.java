package nl.jeroenhero.TrainingPvP.Inventories;

import nl.jeroenhero.TrainingPvP.Arenas.Arena;
import nl.jeroenhero.TrainingPvP.Arenas.ArenaManager;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.Match;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import nl.jeroenhero.TrainingPvP.Utils.ChatRequest;
import nl.jeroenhero.TrainingPvP.Utils.ExecutableItemstack;
import nl.jeroenhero.TrainingPvP.Utils.ItemBuilder;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;

/**
 * Created by Jeroen on 20:13 19-7-2016.
 */
public class ArenaGUI implements Listener{

    private Player player;
    private ArenaGUI instance;

    public ArenaGUI(Player player) {
        this.player = player;
        Bukkit.getPluginManager().registerEvents(this, TrainingPvP.getInstance());
        instance = this;
    }

    public void open() {
        Inventory inv = Bukkit.createInventory(null, 9, ChatColor.GREEN + "Arena Setup");
        ItemStack it = new ItemBuilder(Material.ANVIL).setTitle("Create a new arena").setAmount(1).build();
        ExecutableItemstack exIt = new ExecutableItemstack(it, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                player.sendMessage(Config.prefix + ChatColor.GREEN + "Creating a new arena. Type the name of the new arena in chat! " + ChatColor.RED + "Type cancel to cancel the arena creation.");
                player.closeInventory();
                ChatRequest cr = new ChatRequest(player, new ChatRequest.ChatAction() {
                    @Override
                    public void onChat(String message) {
                        if(message.startsWith("cancel")) {
                            player.sendMessage(Config.prefix + "Cancelled.");
                            HandlerList.unregisterAll(instance);
                            return;
                        }
                        player.sendMessage(Config.prefix + ChatColor.RED + "Creating an arena called: " + message);
                        ServiceLocator.getArenaManager().createArenaConfig(message, player);
                        HandlerList.unregisterAll(instance);
                    }
                }, true);

            }
        }, inv.getName());
        inv.setItem(0, it);

        ItemStack arenaListItem = new ItemBuilder(Material.DIAMOND).setTitle(ChatColor.GREEN + "Arena List").addLore(ChatColor.LIGHT_PURPLE + "A list of all arena's!").setAmount(1).build();
        ExecutableItemstack arIt = new ExecutableItemstack(arenaListItem, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                player.closeInventory();
                final Inventory inv2 = Bukkit.createInventory(null, 54, "Arena List");
                File folder = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/");
                int size = folder.listFiles().length;
                int pageSize = 0;
                int currentNumber = 0;
                File[] files = folder.listFiles();
                for(final File f : files) {
                    if (f.getName().endsWith(".yml")) {
                        final FileConfiguration config = YamlConfiguration.loadConfiguration(f);
                        String name = config.getString("name");
                        boolean setup = config.contains("spawns");
                        if(setup) {
                            if(config.getStringList("spawns").size() < 2) {
                                setup = false;
                            }
                        }
                        ItemBuilder it = new ItemBuilder(Material.WOOL).setAmount(1).setTitle(ChatColor.GREEN + name).addLore(ChatColor.DARK_PURPLE + "Click to view this Arena or make changes.").setDyeColor(DyeColor.GREEN);
                        if (!setup) {
                            it.setDyeColor(DyeColor.RED);
                        }
                        ItemStack item = it.build();
                        ExecutableItemstack exIt = new ExecutableItemstack(item, player, new ExecutableItemstack.ClickAction() {
                            @Override
                            public void onClick(InventoryClickEvent event) {
                                player.closeInventory();
                                openArenaOptionSelctor(player, config, f);
                            }
                        }, inv2.getName());
                        if (currentNumber < 53) {
                            inv2.setItem(currentNumber, item);
                            currentNumber++;
                        } else {

                        }
                    }
                }
                Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
                    @Override
                    public void run() {
                        player.openInventory(inv2);
                    }
                });

            }
        }, inv.getTitle());
        inv.setItem(1, arenaListItem);

        ItemBuilder reloadArenaBuilder = new ItemBuilder(Material.BEACON).setTitle(ChatColor.GREEN + "Reload all arena files / settings");
        ItemStack reloadArenaItem = reloadArenaBuilder.build();

        ExecutableItemstack reloadArenaEx = new ExecutableItemstack(reloadArenaItem, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                player.closeInventory();
                ServiceLocator.getArenaManager().reload();
                player.sendMessage(Config.prefix + ChatColor.GREEN + "Arena's have been reloaded!");
            }
        }, inv.getName());

        inv.setItem(2, reloadArenaItem);

        ItemBuilder setServerSpawnBuilder = new ItemBuilder(Material.LEATHER_BOOTS).setTitle(ChatColor.GREEN + "Set the server spawn").setAmount(1);
        ItemStack spawnItem = setServerSpawnBuilder.build();

        ExecutableItemstack spawnStack = new ExecutableItemstack(spawnItem, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                player.closeInventory();
                ServiceLocator.getArenaManager().setServerLocation(event.getWhoClicked().getLocation(), "spawn");
                player.sendMessage(Config.prefix + "Server spawn has been set!");
            }
        }, inv.getName());

        inv.setItem(3, spawnItem);

        player.openInventory(inv);

        ItemBuilder setServerKitEditBuilder = new ItemBuilder(Material.SHEARS).setTitle(ChatColor.GREEN + "Set the kit edit location").setAmount(1);
        ItemStack kitEditItem = setServerKitEditBuilder.build();

        ExecutableItemstack kitStack = new ExecutableItemstack(kitEditItem, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                player.closeInventory();
                ServiceLocator.getArenaManager().setServerLocation(event.getWhoClicked().getLocation(), "kiteditlocation");
                player.sendMessage(Config.prefix + "Kit edit location has been set!");
            }
        }, inv.getName());

        inv.setItem(4, kitEditItem);

        ItemBuilder resetAllBuilder = new ItemBuilder(Material.WOOD_HOE).setTitle(ChatColor.RED + "Remove ALL Arena's!").addLore(ChatColor.RED + "This can NOT be undone!").addLore(ChatColor.GREEN + "Requires a password.").setAmount(1);
        ItemStack resetAllItem = resetAllBuilder.build();

        ExecutableItemstack resetAllStack = new ExecutableItemstack(resetAllItem, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                player.closeInventory();
                player.sendMessage(Config.prefix + "You have to enter the password in order to reset all arena's. Please type the password in chat or type cancel to cancel the resetting.");
                ChatRequest chatRequest = new ChatRequest(player, new ChatRequest.ChatAction() {
                    @Override
                    public void onChat(String message) {
                        if(message.equals(Config.commandPass)) {
                            player.sendMessage(ChatColor.RED + "Password correct! All arena's will now be removed.");
                            for(Match m : Match.matches) {
                                m.end();
                            }
                            File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/");
                            if(!f.exists()) {
                                return;
                            }
                            for(File fz : f.listFiles()){
                                player.sendMessage(Config.prefix + "Deleted " + fz.getName());
                                fz.delete();
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "Incorrect password. Cancelled arena removal.");
                        }
                    }
                }, true);
            }
        }, inv.getName());

        inv.setItem(5, resetAllItem);

        player.openInventory(inv);

    }

    private void openArenaOptionSelctor(final Player p, final FileConfiguration arenaFile, final File f) {
        if(arenaFile == null || f == null || !f.exists()) {
            p.sendMessage(Config.prefix + "This arena file doesn't exist anymore! Anything happened?");
            return;
        }
        final Inventory inv = Bukkit.createInventory(null, 18, "Arena Options");
        boolean setup = arenaFile.contains("spawns");
        Arena arena = null;
        for(Arena a: ArenaManager.arenas) {
            if(a.getName().equals(arenaFile.getString("name"))) {
                arena = a;
            }
        }
        if(arena == null) {
            setup = false;
        }
        if(setup && arena.getSpawns().size() < 2)
            setup = false;
        ItemBuilder guiItemBuilder = null;
        if(setup) {
            if(arena.isTempDisabled()) {
                ItemBuilder builder = new ItemBuilder(Material.WOOL).setDyeColor(DyeColor.ORANGE).setTitle("Arena is correctly setup but disabled!");
                guiItemBuilder = builder;
            }
            else {
                ItemBuilder builder = new ItemBuilder(Material.WOOL).setDyeColor(DyeColor.GREEN).setAmount(arena.getCurrentMatches().size()).setTitle("Arena is correctly setup and active!").addLore(ChatColor.GREEN + "Active Matches: " + arena.getCurrentMatches().size());
                guiItemBuilder = builder;
            }
        }
        else {
            ItemBuilder builder = new ItemBuilder(Material.WOOL).setDyeColor(DyeColor.RED).setAmount(1).setTitle("Arena isn't setup yet!");
            if(!arenaFile.contains("spawns")) {
                builder.addLore(ChatColor.RED + "No spawns have been setup!");
            }
            else {
                if(arenaFile.getStringList("spawns").size() == 1) {
                    builder.addLore(ChatColor.RED + "Only 1 spawn has been setup!");
                    builder.addLore(ChatColor.RED + "You need at least 2 spawnpoints!");
                }
            }

            guiItemBuilder = builder;
        }
        guiItemBuilder.addLore(ChatColor.GREEN + "Created by: " + arenaFile.getString("creator"));
        guiItemBuilder.addLore(ChatColor.GREEN + "Created on: " + arenaFile.getString("createDate"));
        inv.setItem(0, guiItemBuilder.build());


        final Arena ar = arena;

        ItemBuilder addSpawnItemBuilder = new ItemBuilder(Material.WOOD_DOOR).setTitle(ChatColor.GREEN + "Add spawn").addLore("Uses your current location.");
        if(arena != null) {
            addSpawnItemBuilder.addLore(ChatColor.GREEN + "Current spawns: " + arena.getSpawns().size());
        }
        ItemStack item = addSpawnItemBuilder.build();

        ExecutableItemstack exIt = new ExecutableItemstack(item, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                event.getWhoClicked().closeInventory();
                ServiceLocator.getArenaManager().addSpawn(arenaFile.getString("name"), player.getLocation());
                if(ar != null) {
                    ar.addSpawn(player.getLocation());
                }
                else {
                    ServiceLocator.getArenaManager().loadArena(new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/", arenaFile.getName()));
                }
                player.sendMessage(Config.prefix + "Spawn has been added!");

            }
        }, inv.getName());
        inv.setItem(1, item);

        ItemBuilder removeSpawnItemBuilder = new ItemBuilder(Material.TRAP_DOOR).setTitle(ChatColor.RED + "Remove spawn").addLore("Uses your current location");
        if(arena != null) {
            removeSpawnItemBuilder.addLore(ChatColor.GREEN + "Current spawns: " + arena.getSpawns().size());
        }
        ItemStack item2 = removeSpawnItemBuilder.build();

        new ExecutableItemstack(item2, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                event.getWhoClicked().closeInventory();
                ServiceLocator.getArenaManager().removeSpawn(arenaFile.getString("name"), player.getLocation());
                if(ar == null) {
                    player.sendMessage(Config.prefix + "No spawns set up yet! Nothing to remove!");
                    return;
                }
                boolean b = ar.removeSpawn(player.getLocation());
                if(b) {
                    player.sendMessage(Config.prefix + "Spawnpoint removed!");
                }
                else {
                    player.sendMessage(Config.prefix + "Couldn't find a spawnpoint here?");
                }
            }
        }, inv.getName());
        inv.setItem(2, item2);

        ItemBuilder viewSpawnItemBuilder = new ItemBuilder(Material.DIAMOND).setTitle(ChatColor.GREEN + "Show all spawns for this arena!");
        if(arena != null) {
            viewSpawnItemBuilder.addLore(ChatColor.GREEN + "Current spawns: " + arena.getSpawns().size());
        }
        ItemStack item3 = viewSpawnItemBuilder.build();

        new ExecutableItemstack(item3, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                if(ar == null || ar.getSpawns().size() == 0) {
                    player.sendMessage(Config.prefix + "This arena doesn't have any spawnpoints yet!");
                    return;
                }
                for(Location s : ar.getSpawns()) {
                    Block b = s.getBlock();
                    p.sendBlockChange(s, Material.BEACON, (byte)1);
                }
                event.getWhoClicked().closeInventory();
            }
        }, inv.getName());
        inv.setItem(3, item3);

        ItemBuilder tpSpawnItemBuilder = new ItemBuilder(Material.LEATHER_BOOTS).setTitle("Teleport to the arena").addLore(ChatColor.GREEN + "Teleport to the create-location").addLore(ChatColor.GREEN + "of this arena.");

        ItemStack item4 = tpSpawnItemBuilder.build();

        new ExecutableItemstack(item4, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                if(ar != null) {
                    event.getWhoClicked().teleport(ar.getCreateLocation());
                }
                else {
                    Location loc = ServiceLocator.getArenaManager().locationFromString(arenaFile.getString("createLocation"));
                    event.getWhoClicked().teleport(loc);
                }
            }
        }, inv.getName());

        inv.setItem(4, item4);

        ItemBuilder removeArenaBuilder = new ItemBuilder(Material.WOOL).setDyeColor(DyeColor.RED).setTitle(ChatColor.RED + "Remove this arena").setAmount(1);

        ItemStack item5 = removeArenaBuilder.build();

        new ExecutableItemstack(item5, player, new ExecutableItemstack.ClickAction() {
            @Override
            public void onClick(InventoryClickEvent event) {
                player.sendMessage(Config.prefix + "Removing this arena :C");
                f.delete();
                ServiceLocator.getArenaManager().removeArena(arenaFile.getName());
                player.sendMessage(Config.prefix + "Arena removed.");
                player.closeInventory();
            }
        }, inv.getName());

        inv.setItem(5, item5);

        Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                player.openInventory(inv);
            }
        });

    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        if(e.getInventory() == null || e.getInventory().getName() == null) {
            return;
        }
        switch (ChatColor.stripColor(e.getInventory().getName())) {
            case "Arena Options":
                e.setCancelled(true);
            case "Arena List":
                e.setCancelled(true);
            case "Arena Setup":
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        if(e.getPlayer() == player) {
            HandlerList.unregisterAll(this);
            player = null;
            instance= null;
        }
    }
}
