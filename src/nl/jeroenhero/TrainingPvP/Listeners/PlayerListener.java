package nl.jeroenhero.TrainingPvP.Listeners;

import nl.jeroenhero.TrainingPvP.Arenas.ArenaManager;
import nl.jeroenhero.TrainingPvP.Enums.MatchStage;
import nl.jeroenhero.TrainingPvP.Events.GameEndEvent;
import nl.jeroenhero.TrainingPvP.Events.GameResetEvent;
import nl.jeroenhero.TrainingPvP.Objects.*;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jeroen on 16:01 26-10-2016.
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        if(e.getEntity() != null && e.getEntity() instanceof Player) {
            Player player = e.getEntity();
            PlayerData pd = PlayerData.getPlayerData(player);
            if(pd.getCurrentMatch() != null) {
                Match m = pd.getCurrentMatch();
                Team t = pd.getCurrentTeam();
                t.setAlive(player, false);
                m.update();
                e.setDeathMessage(null);
                for(ItemStack it : e.getDrops()) {
                    if(!it.hasItemMeta())
                        continue;
                    if(it.getItemMeta().getLore() == null)
                        continue;
                    ItemMeta itm = it.getItemMeta();
                    List<String> lore = itm.getLore();
                    lore.add("TrainingPvPID: " + m.getMatchID().toString());
                    itm.setLore(lore);
                    it.setItemMeta(itm);
                }
            }
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        PlayerData pd = PlayerData.getPlayerData(e.getPlayer());
        if(Config.teleportToSpawnOnJoin) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
                @Override
                public void run() {
                    player.teleport(ArenaManager.serverSpawn);
                }
            }, 5L);
        }
        if(pd.isLeft()) {
            if(!Config.teleportToSpawnOnJoin)
                player.teleport(ArenaManager.serverSpawn);
            ServiceLocator.getInventorySaver().setOldInventory(player);
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();

        PlayerData pd = PlayerData.getPlayerData(player);
        if(pd.getStats() != null)
            pd.getStats().save();
        if(pd.getCurrentMatch() == null)
            return;
        player.getInventory().clear();
        for(ItemStack it : player.getInventory().getArmorContents()) {
            player.getInventory().remove(it);
        }

        System.out.println(player.getName() + " has logged off! Saved stats to the database.");
        Match m = pd.getCurrentMatch();
        for(Team team : m.getTeams()) {
            if(team.players.contains(e.getPlayer())) {
                team.setAlive(player, false);
                team.players.remove(player);
            }
        }
        m.update();
        pd.setLeft(true);
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        Player pl = null;
        if(e.getEntity() instanceof Player)
            pl = (Player) e.getEntity();
        if(e.getDamager() instanceof Player && pl != null)
            pl = (Player) e.getDamager();
        if(pl != null) {
            PlayerData pd = PlayerData.getPlayerData(pl);
            if(pd.getCurrentMatch() != null) {
                Match m = pd.getCurrentMatch();
                if(m.getStage() == MatchStage.PREPARE) {
                    e.setCancelled(true);
                    pl.sendMessage(Config.prefix + LangConfig.PVP_DISABLED.getString());
                }
            }
        }
    }

    private boolean isInGame(Player player) {
        return PlayerData.getPlayerData(player).getCurrentMatch() != null;
    }

    private Match getGame(Player player) {
        if(isInGame(player))
            return PlayerData.getPlayerData(player).getCurrentMatch();
        else
            return null;
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if(isInGame(e.getPlayer()))
            e.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e){
        if(isInGame(e.getPlayer()))
            e.setCancelled(true);
    }

    private HashMap<Item, Player> itemMap = new HashMap<Item, Player>();
    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        if(isInGame(e.getPlayer())) {
            itemMap.put(e.getItemDrop(), e.getPlayer());
        }
    }

    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent e) {
        if(e.getItem() != null) {
            if(itemMap.containsKey(e.getItem())) {
                Player dropper = itemMap.get(e.getItem());
                if(isInGame(dropper)) {
                    if(!isInGame(e.getPlayer())) {
                        e.setCancelled(true);
                    }
                    if(getGame(dropper) != getGame(e.getPlayer())) {
                        return;
                    }
                }
            }
            else {
                if(!isInGame(e.getPlayer()))
                    return;
                if(e.getItem().getItemStack() == null)
                    return;
                ItemStack it = e.getItem().getItemStack();
                if(it.getItemMeta() ==null)
                    return;
                ItemMeta itm = it.getItemMeta();
                if(!itm.hasLore())
                    return;
                List<String> lore = itm.getLore();
                for(String s : itm.getLore()) {
                    if(s.equalsIgnoreCase("TrainingPvPID: " + PlayerData.getPlayerData(e.getPlayer()).getCurrentMatch().getMatchID().toString()))
                        lore.remove(s);
                }
            }
        }
    }

    @EventHandler
    public void onGameEnd(GameEndEvent e) {
        for(Entity entity : e.getArena().getSpawns().get(0).getWorld().getEntities()) {
            if(entity instanceof Item) {
                Item it = (Item) entity;
                if(it.getItemStack() != null){
                    ItemStack item = it.getItemStack();
                    if(!item.hasItemMeta())
                        continue;
                    ItemMeta itm = item.getItemMeta();
                    List<String> lore = itm.getLore();
                    for(String s : lore) {
                        if(s.equalsIgnoreCase("TrainingPvPID: " + e.getMatch().getMatchID().toString()))
                            it.remove();
                    }
                }
            }
        }
    }

    @EventHandler
    public void onGameReset(GameResetEvent e) {
        List<Item> remove = new ArrayList<Item>();
        for(Item it : itemMap.keySet()) {
            Player player = itemMap.get(it);
            if(!player.isOnline()) {
                remove.add(it);
            }
            else {
                if(e.getPlayerUUIDs().contains(player.getUniqueId())) {
                    remove.add(it);
                }
            }
        }

        for(Item it : remove) {
            itemMap.remove(it);
            it.remove();
        }
    }
}
