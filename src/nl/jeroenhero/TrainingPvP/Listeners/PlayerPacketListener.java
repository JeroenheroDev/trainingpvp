package nl.jeroenhero.TrainingPvP.Listeners;

import nl.jeroenhero.TrainingPvP.Objects.EntityHider;
import nl.jeroenhero.TrainingPvP.Objects.Match;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Jeroen on 11:42 15-7-2016.
 */
public class PlayerPacketListener implements Listener{

    private EntityHider hider;

    public PlayerPacketListener() {
        hider = new EntityHider(TrainingPvP.getInstance(), EntityHider.Policy.BLACKLIST);
    }

    private boolean isInGame(Player player) {
        return PlayerData.getPlayerData(player).getCurrentMatch() != null;
    }
    private Match getGame(Player player) {
        return PlayerData.getPlayerData(player).getCurrentMatch();
    }


    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntitySpawn(EntitySpawnEvent e) {
        if(e.isCancelled()) { return; }
        if(e.getEntity() != null) {
            if(e.getEntity() instanceof Projectile) {
                Projectile projectile = (Projectile) e.getEntity();
                if(projectile.getShooter() != null) {
                    if(projectile.getShooter() instanceof Player) {
                        Player shooter = (Player) projectile.getShooter();
                        if(isInGame(shooter)) {
                            for(Player player : Bukkit.getOnlinePlayers()) {
                                if(isInGame(player)) {
                                    if(getGame(player) != getGame(shooter)) {
                                        hider.hideEntity(player, e.getEntity());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemDrop(PlayerDropItemEvent e) {
        if(isInGame(e.getPlayer())) {
            Match m = getGame(e.getPlayer());
            for(Player t : Bukkit.getOnlinePlayers()) {
                if(isInGame(t)) {
                    if(getGame(t)!=e.getPlayer()) {
                        hider.hideEntity(t, e.getItemDrop());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent event) {
        if(event.getItem() != null) {
            Item it = event.getItem();
            ItemStack itm = it.getItemStack();
            ItemMeta its = itm.getItemMeta();
            List<String> lore = its.getLore();
            for(String s : lore) {
                if(lore.contains("[TrainingPvP]")) {
                    lore.remove(s);
                }
            }
            its.setLore(lore);
            itm.setItemMeta(its);
            it.setItemStack(itm);
        }
    }

    @EventHandler
    public void onItemSpawn(ItemSpawnEvent e) {
        Item it = e.getEntity();
        if(it != null) {
            if(it.getItemStack() != null) {
                ItemStack its = it.getItemStack();
                if(its.getItemMeta() != null) {
                    ItemMeta itm = its.getItemMeta();
                    if(itm.getLore() == null) {
                        return;
                    }
                    List<String> lore = itm.getLore();
                    for(String s : lore) {
                        if(s.startsWith("[TrainingPvP]")) {
                            String playerName = s.split(" ")[1];
                            if(Bukkit.getPlayer(playerName) != null) {
                                Player player = Bukkit.getPlayer(playerName);
                                if(isInGame(player)) {
                                    Match m = getGame(player);

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent e) {
        if(isInGame(e.getEntity())) {
            Player player = e.getEntity();
            Match m = getGame(player);
            for(ItemStack it : e.getDrops()) {
                if(it != null) {
                    if(it.getItemMeta() != null) {
                        ItemMeta itm = it.getItemMeta();
                        List<String> lore = itm.getLore();
                        lore.add("[TrainingPvP]Droppedby " + e.getEntity().getName());
                        itm.setLore(lore);
                    }
                }
            }

        }
    }

    private void hideEntityForNotInGame(Entity it, Match m) {
        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            if(m.getAllPlayers().contains(player))
                continue;
            hider.hideEntity(player, it);
        }
    }

    private HashMap<UUID, ArrayList<Player>> hiddenList = new HashMap<UUID, ArrayList<Player>>();
    private void hidePlayerForNotInGame(Player hidden, Match m) {
        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            if(m.getAllPlayers().contains(player))
                continue;
            if(!hiddenList.keySet().contains(player.getUniqueId())) {
                ArrayList<Player> nl = new ArrayList<Player>();
                hiddenList.put(player.getUniqueId(), nl);
            }
            ArrayList<Player> nl = hiddenList.get(player.getUniqueId());
            nl.add(hidden);
            hiddenList.put(player.getUniqueId(), nl);
            player.hidePlayer(hidden);
        }
    }

    private void unhideAll(Player hidden, Match m) {
    }



}
