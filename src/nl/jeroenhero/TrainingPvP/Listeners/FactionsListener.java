package nl.jeroenhero.TrainingPvP.Listeners;

import com.massivecraft.factions.event.EventFactionsPvpDisallowed;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Jeroen on 15:48 27-12-2016.
 */
public class FactionsListener implements Listener {

    public FactionsListener() {
        if(Config.factionsHook == false)
            return;
        if(Bukkit.getPluginManager().getPlugin("Factions") != null) {
            Bukkit.getPluginManager().registerEvents(this, TrainingPvP.getInstance());
            System.out.println("Succesfully hooked into Factions!");
        }
    }

    private boolean isInGame(Player player) {
        return PlayerData.getPlayerData(player).getCurrentMatch() != null;
    }

    @EventHandler
    public void onFactionsPvPDissalow(EventFactionsPvpDisallowed event) {
        if(isInGame(event.getAttacker())&&isInGame(event.getDefender())) {
            event.getEvent().setCancelled(false);
        }
    }
}
