package nl.jeroenhero.TrainingPvP.Hooks;

import me.iiSnipez.CombatLog.CombatLog;
import me.iiSnipez.CombatLog.Events.PlayerTagEvent;
import nl.jeroenhero.TrainingPvP.Objects.BattleQueue;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Jeroen on 12:52 29-12-2016.
 */
public class CombatLogHook implements Listener {

    private static boolean enabled = false;

    public CombatLogHook() {
        if(Bukkit.getPluginManager().getPlugin("CombatLog") != null) {
            enabled = true;
        }
        if(enabled)
            Bukkit.getPluginManager().registerEvents(this, TrainingPvP.getInstance());
    }

    @EventHandler
    public void onTag(PlayerTagEvent e) {
        Player player = e.getDamager();
        PlayerData pd = PlayerData.getPlayerData(player);
        if(pd.getCurrentMatch() != null)
            e.setCancelled(true);
        if(pd.isInQueue()) {
            if(!e.isCancelled()) {
                BattleQueue.removeFromQueue(player);
            }
        }
        Player target = e.getDamagee();
        PlayerData pd2 = PlayerData.getPlayerData(target);
        if(pd2.getCurrentMatch() != null)
            e.setCancelled(true);
        if(pd2.isInQueue()) {
            if(!e.isCancelled()) {
                BattleQueue.removeFromQueue(target);
            }
        }
    }

    public static boolean isTagged(Player player) {
        if(!enabled)
            return false;
        CombatLog c = (CombatLog) Bukkit.getPluginManager().getPlugin("CombatLog");
        return c.taggedPlayers.containsKey(player.getName());
    }



}
