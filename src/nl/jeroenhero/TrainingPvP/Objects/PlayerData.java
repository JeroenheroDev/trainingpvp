package nl.jeroenhero.TrainingPvP.Objects;

import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.Stats.PlayerStats;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jeroen on 11:12 30-8-2016.
 */
public class PlayerData {

    public static HashMap<UUID, PlayerData> dataMap = new HashMap<UUID, PlayerData>();

    public static PlayerData getPlayerData(Player player) {
        return getPlayerData(player.getUniqueId());
    }

    public static PlayerData getPlayerData(UUID u) {
        if(dataMap.containsKey(u)) {
            return dataMap.get(u);
        }
        return new PlayerData(Bukkit.getPlayer(u));
    }

    private Match currentMatch;
    private Team currentTeam;
    private Party party;
    private Player player;
    private HashMap<UUID, DuelInvite> duelInvited = new HashMap<UUID, DuelInvite>();
    private Player duelInviting;
    private PlayerStats stats;
    private long newRankedMatch = 0;
    private boolean left = false;

    public PlayerData(Player player) {
        setPlayer(player);
        dataMap.put(player.getUniqueId(), this);
        ServiceLocator.getStatsDatabase().loadStatsAsync(player);
    }

    public boolean isEditingKit() {
        return ServiceLocator.getEditableKitManager().getEditingKits().contains(player);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public Team getCurrentTeam() {
        return currentTeam;
    }

    public void setCurrentTeam(Team currentTeam) {
        this.currentTeam = currentTeam;
    }

    public Match getCurrentMatch() {
        return currentMatch;
    }

    public void setCurrentMatch(Match currentMatch) {
        this.currentMatch = currentMatch;
    }

    public boolean isInQueue() {
        return BattleQueue.isInQueue(this.getPlayer());
    }

    public void inviteForDuel(Player player, DuelInvite invite) {
        duelInvited.put(player.getUniqueId(), invite);
    }

    public boolean isInvitedForDuel(UUID u) {
        return duelInvited.containsKey(u);
    }

    public DuelInvite getInvite(UUID u) {
        return duelInvited.get(u);
    }

    public void removeInvite(UUID u) {
        duelInvited.remove(u);
    }

    public PlayerStats getStats() {
        return stats;
    }

    public void setStats(PlayerStats stats) {
        this.stats = stats;
    }

    public Player getDuelInviting() {
        return duelInviting;
    }

    public void setDuelInviting(Player duelInviting) {
        this.duelInviting = duelInviting;
    }

    public long getNextRankedMatchMili() {
        return newRankedMatch;
    }

    public void setNextRankedMatchMili(long newRankedMatch) {
        this.newRankedMatch = newRankedMatch;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }
}
