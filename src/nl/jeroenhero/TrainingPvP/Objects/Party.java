package nl.jeroenhero.TrainingPvP.Objects;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Jeroen on 11:16 30-8-2016.
 */
public class Party {

    public static ArrayList<Party> parties = new ArrayList<Party>();

    public Party() {
        parties.add(this);
    }

    private Player leader;
    private ArrayList<Player> members = new ArrayList<Player>();
    private ArrayList<UUID> invited = new ArrayList<UUID>();

    public ArrayList<Player> getMembers() {
        return members;
    }

    public boolean isMember(Player player) {
        return members.contains(player);
    }

    public void addInvite(UUID u) {
        invited.add(u);
    }

    public boolean isInvited(UUID u) {
        return invited.contains(u);
    }

    public void unInvite(UUID u) {
        invited.remove(u);
    }

    public void addMember(Player player) {
        members.add(player);
        PlayerData pd = PlayerData.getPlayerData(player);
        pd.setParty(this);
        if(leader != player)
            sendAllMessage(ChatColor.YELLOW + player.getName() + ChatColor.GREEN + " has joined your party!");
    }

    public Player getLeader() {
        return leader;
    }

    public void setLeader(Player leader) {
        this.leader = leader;
    }

    public void removePlayer(Player player) {
        members.remove(player);
        player.sendMessage(Config.prefix + LangConfig.PARTY_LEFT.getString());
        sendAllMessage(LangConfig.PARTY_LEFT_OTHER.getString(player));
        PlayerData.getPlayerData(player).setParty(null);
    }

    public void kickPlayer(Player player) {
        members.remove(player);
        sendAllMessage(LangConfig.OTHER_KICKED.getString(player));
        player.sendMessage(Config.prefix + LangConfig.PARTY_KICKED.getString());
        PlayerData.getPlayerData(player).setParty(null);
    }

    public void disband() {
        for(Player player : members) {
            PlayerData pd = PlayerData.getPlayerData(player);
            pd.setParty(null);
            player.sendMessage(Config.prefix + LangConfig.PARTY_DELETED.getString());
        }
        this.leader = null;
        this.members.clear();
        parties.remove(this);
    }

    public void sendAllMessage(String s) {
        for(Player player : members) {
            player.sendMessage(Config.prefix + s);
        }
    }

}
