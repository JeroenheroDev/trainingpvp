package nl.jeroenhero.TrainingPvP.Objects;

import nl.jeroenhero.TrainingPvP.Kits.Kit;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeroen on 09:04 30-8-2016.
 */
public class BattleQueue implements Listener{

    public static ArrayList<Queue> queueList = new ArrayList<Queue>();

    public static Team getQueue(BattleMode mode, String kit, boolean ranked) {
        Queue qc = null;
        for(Queue q : queueList) {
            if(q.mode == mode.getName() && q.kit == kit && q.ranked == ranked) {
                qc = q;
            }
        }
        queueList.remove(qc);
        if(qc == null)
            return null;
        return qc.team;
    }

    public static void resetQueue(Team team) {
        Queue qc = null;
        for(Queue q : queueList) {
            if(q.team == team) {
                qc = q;
            }
        }
        queueList.remove(qc);
    }

    public static void addToQueue(BattleMode mode, final Team team, Kit kit, boolean ranked) {
        Queue queue = new Queue(team, mode.getName(), kit.getName());
        queue.ranked = ranked;
        queueList.add(queue);
        Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                for(Player player : team.players) {
                    String msg = "tellraw %PLAYER% {\"text\":\"%MESSAGE%\",\"color\":\"green\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/battle leave\"}}";
                    msg = msg.replace("%PLAYER%", player.getName());
                    msg = msg.replace("%MESSAGE%", LangConfig.CLICK_TO_LEAVE_QUEUE.getString());
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), msg);
                }
            }
        },5L);

    }

    public static void removeFromQueue(Player player) {
        for(Queue queue : queueList) {
            final Team team = queue.team;
            if(team.players.contains(player)) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
                    @Override
                    public void run() {
                        resetQueue(team);
                    }
                });
                if(team.players.size() > 0){
                    for(Player teamPlayer : team.players) {
                        teamPlayer.sendMessage(Config.prefix + LangConfig.PVP_KICK_QUEUE.getString());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        for(Queue queue : queueList) {
            Team team = queue.team;
            if(team.players.contains(e.getPlayer())) {
                resetQueue(team);
                team.players.remove(e.getPlayer());
                if(team.players.size() > 0){
                    for(Player player : team.players) {
                        player.sendMessage(Config.prefix + LangConfig.PARTY_LEFT_OTHER.getString(e.getPlayer()));
                        player.sendMessage(Config.prefix + LangConfig.QUEUE_LEFT.getString());
                    }
                }
            }
        }
    }

    public static boolean isInQueue(Player player) {
        for(Queue queue : queueList) {
            Team team = queue.team;
            if(team.players.contains(player)) {
                return true;
            }
        }
        return false;
    }

    public static class Queue {

        public Team getTeam(){
            return team;
        }

        public String getMode() {
            return mode;
        }

        public String getKit() {
            return kit;
        }

        Team team;
        String mode;
        String kit;
        boolean ranked = false;

        public Queue(Team team, String mode, String kit) {
            this.team =team;
            this.mode = mode;
            this.kit = kit;
        }
    }
}
