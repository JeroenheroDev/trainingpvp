package nl.jeroenhero.TrainingPvP.Objects;

import nl.jeroenhero.TrainingPvP.Kits.Kit;
import org.bukkit.entity.Player;

/**
 * Created by Jeroen on 11:46 30-9-2016.
 */
public class DuelInvite {

    public Player inviter;
    public Player invited;
    public Kit kit;

    public DuelInvite(Player inviter, Player invited, Kit kit) {
        this.kit = kit;
        this.invited = invited;
        this.inviter = inviter;
    }
}
