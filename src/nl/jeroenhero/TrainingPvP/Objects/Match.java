package nl.jeroenhero.TrainingPvP.Objects;

import com.google.common.util.concurrent.Service;
import nl.jeroenhero.TrainingPvP.Arenas.Arena;
import nl.jeroenhero.TrainingPvP.Arenas.ArenaManager;
import nl.jeroenhero.TrainingPvP.Enums.MatchStage;
import nl.jeroenhero.TrainingPvP.Enums.RankedType;
import nl.jeroenhero.TrainingPvP.Kits.ConfigKit;
import nl.jeroenhero.TrainingPvP.Kits.Kit;
import nl.jeroenhero.TrainingPvP.Kits.KitManager;
import nl.jeroenhero.TrainingPvP.Modes.Mode1v1;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.Stats.EloCalculator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import nl.jeroenhero.TrainingPvP.Utils.NameTagUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jeroen on 11:17 15-7-2016.
 */
public class Match {

    public static ArrayList<Match> matches = new ArrayList<Match>();
    private BattleMode mode;
    private boolean hasStarted = false;
    private Kit kit;
    private Arena arena;
    private MatchStage stage;
    private boolean ranked = false;
    private UUID matchID;

    private long matchStartMili = 0;

    public void setRanked(Boolean b) {
        ranked=b;
    }

    public boolean isRanked() {
        return ranked;
    }

    private ArrayList<Team> teams;

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public MatchStage getStage() {
        return stage;
    }

    public UUID getMatchID () {
        return matchID;
    }

    public Arena getArena() {
        return arena;
    }

    public Match(BattleMode mode) {
        this.mode = mode;
        matchID = UUID.randomUUID();
    }

    public void start(BattleMode mode, ArrayList<Team> teams, Kit kit) {
        List<Arena> possibleArenaList = new ArrayList<Arena>();
        for(Arena a : ArenaManager.arenas) {
            if(a.isInUse())
                continue;
            if(a.isTempDisabled())
                continue;
            if(a.getSpawns().size() < 2)
                continue;
            if(a.getSpawns().size() < mode.getTeamAmount())
                continue;
            if(a.getModes().size() > 0) {
                for(String s : a.getModes()) {
                    if(s.equalsIgnoreCase(mode.getName()) && !possibleArenaList.contains(a)) {
                        possibleArenaList.add(a);
                    }
                }
            }
            else {
                possibleArenaList.add(a);
            }
        }
        if(possibleArenaList.size() < 1) {
            for(Team t : teams) {
                for(Player p : t.players) {
                    p.sendMessage(Config.prefix + LangConfig.NO_ARENA_AVAILABLE.getString());
                }
            }
            return;
        }
        else {
            Arena a = null;
            int size = possibleArenaList.size();
            a = possibleArenaList.get(new Random().nextInt(size));
            start(mode, teams, a, kit);
        }
    }

    public void start(BattleMode mode, ArrayList<Team> teams, Arena arena, Kit kit) {
        if(hasStarted == true) {
            return;
        }
        matchStartMili = System.currentTimeMillis();
        hasStarted = true;
        this.kit = kit;
        this.arena = arena;
        arena.setInUse(true);
        this.mode = mode;
        this.teams = new ArrayList<Team>();

        if(mode.getTeamAmount() != teams.size()) {
            sendAllMessage(ChatColor.RED + "This gamemode requires " + mode.getTeamAmount() + " teams, but you tried to start it with " + teams.size() + " teams! Please report this error to an administrator!");
            return;
        }
        if(arena == null) {
            sendAllMessage(ChatColor.RED + "Something went wrong while loading this arena!");
            return;
        }
        mode.onStart();
        stage = MatchStage.PREPARE;
        int i = 0;
        boolean listEnemies = false;
        String s = LangConfig.START_GAME_MESSAGE.getString();
        if(s.contains("%MODE%"))
            s = s.replace("%MODE%", mode.getName());
        if(s.contains("%ENEMIES%")) {
            listEnemies = true;
            s = s.replace("%ENEMIES%", "");
        }
        if(s.contains("%ARENA%"))
            s = s.replace("%ARENA%", arena.getName());
        if(s.contains("%MAP%"))
            s = s.replace("%MAP%", arena.getName());
        for(Team team : teams) {
            this.teams.add(team);
            for(Player player : team.players) {
                ServiceLocator.getInventorySaver().saveInventory(player);
                player.closeInventory();
                player.getInventory().clear();
                player.teleport(arena.getSpawns().get(i));
                PlayerData pd = PlayerData.getPlayerData(player);
                pd.setCurrentMatch(this);
                if(kit instanceof ConfigKit) {
                    ServiceLocator.getEditableKitManager().giveEditedKit(player, (ConfigKit)kit);
                }
                else
                    kit.giveItems(player);
                player.sendMessage(Config.prefix + s);
                if(listEnemies) {
                    int color = 0;
                    for(Team enemyTeam : teams) {
                        if(enemyTeam != team) {
                            for(Player enemy : enemyTeam.players) {
                                player.sendMessage(Config.prefix + NameTagUtil.getTeamColor(color) + enemy.getName());
                            }
                            color ++;
                        }
                    }
                }
            }
            i ++;
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                if(stage == MatchStage.PREPARE)
                    stage= MatchStage.INGAME;
            };
        }, 100L);
    }

    public void end() {
        stage = MatchStage.AFTERGAME;
        boolean winner = false;
        boolean multiWinner = false;
        Team winnerTeam = null;
        for(Team team : getTeams()) {
            if(team.getAlivePlayersSize() > 0) {
                winner = true;
                if(winnerTeam != null) {
                    multiWinner = true;
                }
                else
                    winnerTeam = team;
            }
        }
        for(Player player : getAllPlayers()) {
            player.closeInventory();
            player.getInventory().clear();
            player.getInventory().setHelmet(new ItemStack(Material.AIR));
            player.getInventory().setChestplate(new ItemStack(Material.AIR));
            player.getInventory().setLeggings(new ItemStack(Material.AIR));
            player.getInventory().setBoots(new ItemStack(Material.AIR));
            Damageable d = player;
            d.setHealth(d.getMaxHealth());
            for(PotionEffect effect: player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }
            player.setNoDamageTicks(Config.timeAfterGame * 20);
            player.teleport(ArenaManager.serverSpawn);
            if(winner && !multiWinner) {
                if(winnerTeam.players.size() > 1) {
                    String s = "";
                    for(Player players : winnerTeam.players) {
                        s = players.getName() + ", ";
                    }
                    player.sendMessage(Config.prefix + LangConfig.WIN_MESSAGE_MULTIPLE.getString() + " " + s);
                }
                else {
                    player.sendMessage(Config.prefix + LangConfig.WIN_MESSAGE_SOLO.getString(winnerTeam.players.get(0)));
                }
            }
            if(multiWinner && winner) {
                sendAllMessage(Config.prefix + LangConfig.ADMIN_GAME_END.getString());
            }
        }
        if(Config.afterMatchReport) {
            if(winner && !multiWinner) {
                int totKilled = 0;
                for(Team team : getTeams()) {
                    if (team != winnerTeam) {
                        int size = team.players.size();
                        totKilled = totKilled + size;
                    }
                }
                int winnerDeaths = winnerTeam.players.size() - winnerTeam.getAlivePlayersSize();
                String gameTime = LangConfig.MATCH_DURATION.getString();
                long mili = System.currentTimeMillis() - this.matchStartMili;
                long mins = TimeUnit.MILLISECONDS.toMinutes(mili);
                long sec = TimeUnit.MILLISECONDS.toSeconds(mili);
                sec = sec - (mins * 60);
                gameTime = gameTime.replace("%TIME%", ChatColor.YELLOW + "" + mins + ChatColor.GREEN + " minutes, " + ChatColor.YELLOW + sec + ChatColor.GREEN + " seconds");
                for(Player player : getAllPlayers()) {
                    if(!winnerTeam.players.contains(player)) {
                        player.sendMessage(Config.prefix + LangConfig.LOSER_DETAILS.getString());
                        player.sendMessage(LangConfig.ENEMY_HP.getString());
                        for(Player alive : getAlivePlayers()) {
                            player.sendMessage(ChatColor.YELLOW + alive.getName() + " " + ChatColor.GREEN + (int)(alive.getHealth() /2.0) + ChatColor.RED + "❤");
                        }
                    }
                    else {
                        player.sendMessage(Config.prefix + LangConfig.WINNER_DETAILS.getString());
                        player.sendMessage(LangConfig.WINNER_DETAILS_KILLS_MULTI.getString().replace("%KILLED%", totKilled + ""));
                        String ripMsg = LangConfig.WINNER_DETAILS_DEATHS_MULTI.getString().replace("%KILLED%", winnerDeaths + "");
                        if(winnerDeaths == 0) {
                            ripMsg = ripMsg.replace("RIP", "Well Done!");
                        }
                        player.sendMessage(ripMsg);
                    }
                    player.sendMessage(gameTime);
                }

            }
        }
        if(ranked) {
            if(!multiWinner && winnerTeam != null) {
                if(this.teams.size() == 2) {
                    if(this.getAllPlayers().size() == 2) {
                        Player player = winnerTeam.players.get(0);
                        Player loser = null;
                        for(Team team : teams) {
                            if(team.players != null) {
                                if(team.players.size() > 0) {
                                    if(team.players.get(0) != player) {
                                        loser = team.players.get(0);
                                    }
                                }
                            }
                            for(Player pl : team.players) {
                                if(Config.cooldownBetweenRankedEnabled) {
                                    PlayerData pd = PlayerData.getPlayerData(pl);
                                    pd.setNextRankedMatchMili(System.currentTimeMillis() + (Config.cooldownBetweenRankedGames * 1000));
                                }
                            }
                        }
                        if(loser != null) {
                            EloCalculator.calculateElo(player, loser, kit.getName());
                        }
                    }
                }
            }
        }
        if(kit.isOwnItems() == false)
            finish();
        else {
            Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
                @Override
                public void run() {
                    finish();
                }
            },Config.timeAfterGame * 20);
        }
    }

    private void finish() {
        arena.setInUse(false);
        for(Player player : getAllPlayers()) {
            PlayerData pd = PlayerData.getPlayerData(player);
            pd.setCurrentMatch(null);
            ServiceLocator.getInventorySaver().setOldInventory(player);
        }
    }

    public ArrayList<Player> getAllPlayers() {
        ArrayList<Player> players = new ArrayList<Player>();
        for(Team team : getTeams()) {
            for(Player player : team.players) {
                players.add(player);
            }
        }
        return players;
    }

    public ArrayList<Player> getAlivePlayers() {
        ArrayList<Player> players = new ArrayList<Player>();
        for(Team team : getTeams()) {
            for(Player player : team.getAlivePlayers()) {
                players.add(player);
            }
        }
        return players;
    }


    public BattleMode getMode() {
        return mode;
    }

    public Kit getKit() {
        return kit;
    }

    private void sendAllMessage(String s) {
        for(Team t : teams) {
            for(Player p : t.players) {
                p.sendMessage(Config.prefix + s);
            }
        }
    }

    /**
     * Updating is used after a player dies, and it will look if there are still any teams left.
     */
    public void update() {
        int aliveTeams = 0;
        for(Team team : getTeams()) {
            if(team.getAlivePlayersSize() > 0) {
                aliveTeams ++;
            }
        }
        if(aliveTeams < 2) {
            end();
        }
    }
}
