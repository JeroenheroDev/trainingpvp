package nl.jeroenhero.TrainingPvP.Objects;

import nl.jeroenhero.TrainingPvP.Enums.RankedType;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Jeroen on 13:20 18-7-2016.
 */
public abstract class BattleMode {

    public static ArrayList<BattleMode> modes = new ArrayList<BattleMode>();

    private ItemStack guiItem = null;
    private RankedType rankedType = RankedType.BOTH;
    private boolean enabled;

    public static void registerMode(BattleMode mode) {
        try {
            File f = new File(TrainingPvP.getInstance().getDataFolder(), "modes.yml");
            if(!f.exists()) {
                f.createNewFile();
            }
            FileConfiguration config = YamlConfiguration.loadConfiguration(f);
            config.createSection(mode.getName());
            setIfNoDefault(config, mode.getName() + ".enabled", true);
            //config.addDefault(mode.getName() + ".enabled", true);
            setIfNoDefault(config, mode.getName() + ".gui-item-type", 276);
            setIfNoDefault(config, mode.getName() + ".gui-item-name", mode.getName());
            List<String> modeLore = new ArrayList<String>();
            if(mode.isPartyRequired() && mode.isPartySplit()) {
                modeLore.add("Fight against half of your party!");
            }
            else if(mode.isPartyRequired() && !mode.isPartySplit()) {
                modeLore.add("Fight against another party!");
                if(mode.getTeamSize() != -1) {
                    modeLore.add("&4You need " + mode.getTeamSize() + " players in your party!");
                }
            }
            else if(!mode.isPartyRequired() && mode.getTeamSize() == 1) {
                modeLore.add("Fight against another player!");
            }
            else if(!mode.isPartyRequired() && mode.getTeamSize() > 1) {
                modeLore.add("Team up with other non-party players and fight against others!");
            }
            else {
                modeLore.add("Fight against people.");
            }
            modeLore.add("In Game: %INGAME%");
            modeLore.add("In Queue: %QUEUE%");
            setIfNoDefault(config, mode.getName() + ".gui-item-lore", modeLore);
            config.save(f);
            if(config.getBoolean(mode.getName() + ".enabled")) {
                mode.enabled = true;
                modes.add(mode);
                TrainingPvP.getInstance().getLogger().log(Level.INFO, "Succesfully registered the mode: " + mode.getName());
            }
            else {
                mode.enabled = false;
                TrainingPvP.getInstance().getLogger().log(Level.INFO, "I did NOT register the mode: " + mode.getName() + " due to it being disabled in the modes.yml config.");
            }
        }
        catch(Exception e) {
            TrainingPvP.getInstance().getLogger().log(Level.INFO, "I did NOT register the mode: " + mode.getName() + " due to it having an invalid config OR an error occured. Error log:");
            e.printStackTrace();
        }


    }

    private static void setIfNoDefault(FileConfiguration config, String value, Object key) {
        if(!config.contains(value)) {
            config.set(value, key);
        }
    }

    public abstract int getTeamSize();

    public abstract boolean isPartyRequired();

    public abstract boolean isPartySplit();

    public abstract String getName();

    public int getTeamAmount() {
        return 2;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public ItemStack getGUIItem() {
        if(this.guiItem != null) {
            return guiItem.clone();
        }
        File f = new File(TrainingPvP.getInstance().getDataFolder(), "modes.yml");
        if(!f.exists()) {
            f.mkdirs();
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        ItemStack it = new ItemStack(config.getInt(getName() + ".gui-item-type"));
        ItemMeta itm = it.getItemMeta();
        itm.setDisplayName(ChatColor.translateAlternateColorCodes('&',config.getString(getName() + ".gui-item-name")));
        if(!config.contains(getName() + ".gui-item-lore")) {
            it.setItemMeta(itm);
            return it;
        }
        List<String> lore = config.getStringList(getName() + ".gui-item-lore");
        List<String> coloredLore = new ArrayList<String>();
        for(String s : lore) {
            coloredLore.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        itm.setLore(coloredLore);
        it.setItemMeta(itm);
        this.guiItem = it;
        return it.clone();
    }

    public void onStart() {}
}
