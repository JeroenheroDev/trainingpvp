package nl.jeroenhero.TrainingPvP.Objects;

import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Random;

/**
 * Created by Jeroen on 14:33 18-7-2016.
 */
public class Config {


    public static boolean preGameCountdown = false;
    public static boolean partyEnabled = true;
    public static boolean rankedEnabled = true;
    public static boolean duelEnabled = true;
    public static boolean perKitElo = true;
    public static boolean mySQL = false;
    public static String mySQLUserName = "";
    public static String mySQLPassWord = "";
    public static String mySQLIP = "";
    public static String mySQLPort = "3306";
    public static String mySQLDatabase = "";
    public static boolean multipleMatchesInOneArena = true;
    public static String partyCommand;
    public static String prefix = ChatColor.GREEN+ "[" + ChatColor.AQUA + "TrainingPvP" + ChatColor.GREEN + "] ";
    public static String kitCommand;
    public static String duelCommand;
    public static String editKitCommand;
    public static String saveCommand;
    public static boolean editKits;
    public static String commandPass;
    public static int defaultElo = 1000;
    public static int eloKFactor = 32;
    public static boolean factionsHook = false;
    public static int timeAfterGame = 5;
    public static boolean afterMatchReport = true;
    public static boolean teleportToSpawnOnJoin = false;
    public static boolean clearInventoryOnJoin = false;
    public static boolean cooldownBetweenRankedEnabled = false;
    public static int cooldownBetweenRankedGames = 60;
    public static boolean spawnItemsEnabled = false;

    public static void load() {
        FileConfiguration config = TrainingPvP.getInstance().getConfig();
        config.options().copyDefaults(true);
        TrainingPvP.getInstance().saveConfig();
        config.addDefault("commandPassword", "DEFAULT_" + new Random().nextInt(10000));
        config.addDefault("preGameCountdown", true);
        config.addDefault("partyEnabled", true);
        config.addDefault("rankedEnabled", true);
        config.addDefault("duelEnabled", true);
        config.addDefault("perKitElo", true);
        config.addDefault("mySQL", false);
        config.addDefault("mySQLUserName", "USERNAME");
        config.addDefault("mySQLPassWord", "PASSWORD");
        config.addDefault("mySQLIP", "IP");
        config.addDefault("mySQLPort", "3306");
        config.addDefault("mySQLDatabase", "TrainingPvP");
        config.addDefault("multipleMatchesInOneArena", true);
        config.addDefault("prefix", "&b[&aTrainingPvP&b] ");
        config.addDefault("partyCommand", "party");
        config.addDefault("kitCommand", "kit");
        config.addDefault("duelCommand", "duel");
        config.addDefault("editKitCommand", "edit");
        config.addDefault("saveCommand", "save");
        config.addDefault("editKits", true);
        config.addDefault("defaultElo", 1000);
        config.addDefault("eloKFactor", 32);
        config.addDefault("factionsHookEnabled", true);
        config.addDefault("timeAfterGame", 5);
        config.addDefault("afterMatchReport", true);
        config.addDefault("teleportToSpawnOnJoin", false);
        config.addDefault("clearInventoryOnJoin", false);
        config.addDefault("cooldownBetweenRankedGamesEnabled", false);
        config.addDefault("cooldownBetweenRankedGames", 60);
        config.addDefault("spawnItemsEnabled", false);
        TrainingPvP.getInstance().saveConfig();
        commandPass = config.getString("commandPassword");
        preGameCountdown = config.getBoolean("preGameCountdown");
        partyEnabled = config.getBoolean("partyEnabled");
        rankedEnabled = config.getBoolean("rankedEnabled");
        duelEnabled = config.getBoolean("duelEnabled");
        perKitElo = config.getBoolean("perKitElo");
        mySQL = config.getBoolean("mySQL");
        mySQLUserName = config.getString("mySQLUserName");
        mySQLPassWord = config.getString("mySQLPassWord");
        mySQLIP = config.getString("mySQLIP");
        mySQLPort = config.getString("mySQLPort");
        mySQLDatabase = config.getString("mySQLDatabase");
        multipleMatchesInOneArena = config.getBoolean("multipleMatchesInOneArena");
        partyCommand = config.getString("partyCommand");
        prefix = ChatColor.translateAlternateColorCodes('&', config.getString("prefix"));
        kitCommand = config.getString("kitCommand");
        duelCommand = config.getString("duelCommand");
        editKitCommand = config.getString("editKitCommand");
        editKits = config.getBoolean("editKits");
        saveCommand = config.getString("saveCommand");
        defaultElo = config.getInt("defaultElo");
        eloKFactor = config.getInt("eloKFactor");
        factionsHook = config.getBoolean("factionsHookEnabled");
        timeAfterGame = config.getInt("timeAfterGame");
        afterMatchReport = config.getBoolean("afterMatchReport");
        teleportToSpawnOnJoin = config.getBoolean("teleportToSpawnOnJoin");
        clearInventoryOnJoin = config.getBoolean("clearInventoryOnJoin");
        cooldownBetweenRankedEnabled = config.getBoolean("cooldownBetweenRankedGamesEnabled");
        cooldownBetweenRankedGames = config.getInt("cooldownBetweenRankedGames");
        spawnItemsEnabled = config.getBoolean("spawnItemsEnabled");
    }
}
