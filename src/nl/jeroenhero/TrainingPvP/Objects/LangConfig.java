package nl.jeroenhero.TrainingPvP.Objects;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

/**
 * Created by Jeroen on 11:24 15-7-2016.
 */
public enum LangConfig {

    ALREADY_IN_A_GAME("&4You are already in a game!"),
    COMBAT_TAGGED("&aYou are currently combat tagged!"),
    NO_DUEL_WHILE_SPECTATING("&4You can't duel while spectating!"),
    NO_DUEL_WHILE_IN_PARTY("&4You can't do this while you are in a party!"),
    IN_A_QUEUE("&4You can't do this, as you are already in a queue! Leave it first!"),
    OTHER_ALREADY_IN_A_GAME("&d%player% &ais already in a game!"),
    OTHER_ALREADY_WAITING_FOR_GAME("&d%player% &ais already waiting for a game!"),
    DUEL_ACCEPTED("&aYou have accepted the duel!"),
    NOT_INVITED("That player didn't invite you for a duel, or the duel expired! Do /duel to challenge him!"),
    DUEL_UNKNOWN_COMMAND("/duel <Player> || /duel accept <Player>"),
    ALREADY_WAITING("&4You are already waiting for a game!"),
    NO_PERMISSION("&4You don't have the permission to do this!"),
    UNKNOWN_PLAYER("&4This player is unknown!"),
    ALREADY_INVITED("&4You've already invited that player!"),
    DUEL_EXPIRY_WARNING("&4The request expires in 30 seconds!"),
    DUEL_EXPIRED("&4The duel request has expired!"),
    DUEL_CHALLENGE("&a%player% has invited you for a duel! do /duel accept %player% to accept the invite!"),
    DUEL_INVITE_SEND("&aA duel invite has been send!"),
    CLICK_TO_ACCEPT_DUEL("Click here to accept the duel invite against %INVITER%"),
    ELO_UNKNOWN_COMMAND("/elo || /elo <Player>"),
    PARTY_LIST("Party List:"),
    PLAYER_NOT_IN_PARTY("That player isn't in a party!"),
    PLAYER_NOT_IN_YOUR_PARTY("That player is not in your party!"),
    ALREADY_IN_PARTY("You are already in a party! Leave that first!"),
    PARTY_FULL("You can't join that party because it is full!"),
    NOT_INVITED_FOR_PARTY("&aYou haven't been invited for that party!"),
    NOT_IN_PARTY("&aYou are not in a party!"),
    PLAYER_JOINED_PARTY("&a%player% has joined the party!"),
    OTHER_PLAYER_NOT_PARTY_LEADER("&aThat player isn't the party leader!"),
    PARTY_LEADER_IS("&aThe party leader is %player%"),
    NOT_PARTY_LEADER("&4You are not the party leader!"),
    OTHER_ALREADY_IN_PARTY("&aThis player is already in your party!"),
    PARTY_CREATED("&aParty created! Now go invite some players!"),
    PVP_PROTECTION_REMOVED("&aThe PvP protection has now been removed!"),
    GAME_BOSSBAR_MESSAGE("&aYou are fighting on TrainingPvP! Have fun!"),
    NO_KIT_AVAILABLE("&aThere are no kits available for this type!"),
    KIT_SELECTOR_GUI_TITLE("&aKit Selector"),
    MODE_SELECTOR_GUI_TITLE("&aGamemode Selector"),
    DUEL_OPPONENT_OFFLINE("&aThe player you are duelling against went offline!"),
    ADMIN_GAME_END("&aYour game has been ended by an admin!"),
    PARTY_TOO_SMALL("&aYour party needs at least 2 players to do this!"),
    PARTY_TOO_BIG("&aYour party must have 2 players to do this!"),
    JOINED_QUEUE("You have joined the waiting queue!"),
    ALREADY_IN_QUEUE("You are already waiting for a game!"),
    PVP_DISABLED("You can't fight yet! Wait at least 5 seconds after the game started."),
    PLAYER_QUIT("&a%player% has left the game!"),
    NEW_COMMANDS("&a/Battle has been changed to /unranked! Want to do ranked matches? Do /ranked!"),
    NO_ARENA_AVAILABLE("&aThere is currently no arena available matching your requirements. Please contact an administrator."),
    ELO_UPDATE("&aELO after this match: &b%WINNER% &a[%WINNER_ELO%] &a(%WINNER_CHANGE%) &e- &b%LOSER% &a[%LOSER_ELO%] &c(%LOSER_CHANGE%&c)"),
    RANKED_COOLDOWN("&aYou have to wait %time% more seconds before you can do another ranked match!"),
    DUEL_COOLDOWN("&aYou have to wait %time% more seconds before you can do another duel!"),
    PARTY_KICKED("&aYou have been kicked from your party!"),
    OTHER_KICKED("&a%player% has been kicked from your party!"),
    PARTY_LEFT("&aYou left your party!"),
    PARTY_LEFT_OTHER("&a%player% has left your party!"),
    PARTY_DELETED("&aYour party has been deleted!"),
    NOT_IN_QUEUE("&aYou are currently not in a queue!"),
    QUEUE_LEFT("&aYou have left the waiting queue!"),
    UNRANKED_SELECTOR_ITEM("&aUnranked Selector"),
    RANKED_SELECTOR_ITEM("&aRanked Selector"),
    QUEUE_LEAVE_ITEM("&cLeave the waiting queue!"),
    PARTY_CREATE_ITEM("&aCreate a party!"),
    PARTY_VIEW_ITEM("&bShow your party!"),
    PARTY_LIST_ITEM("&bShow all parties!"),
    PARTY_LEAVE_ITEM("&cLeave your party!"),
    CANT_DUEL_YOURSELF("&aYou can't duel yourself silly!"),
    CANT_SELF("&aYou can't do this with yourself!"),
    NO_SPECTATING_WHILE_IN_GAME("&aYou are currently in a game! You can only spectate if you are not in a game!"),
    DUEL_TOGGLED_ON("&aEnabled duel requests!"),
    DUEL_TOGGLED_OFF("&aDisabled duel requests!"),
    OTHER_TOGGLED_REQUESTS("&aCan't send the request as &b%player% &ahas duel requests disabled!"),
    LOGGED_INGAME("&aYou logged off during a game, so you lost!"),
    PARTY_INVITED_MSG("&aYou have been invited for &e%player%&a's party! Do &e/party accept %player% &ato join the party!"),
    PARTY_INVITED_OTHER("&a%player% has been invited for your party!"),
    START_GAME_MESSAGE("&aYou are currently playing %MODE% on %MAP%! Fighting against: %ENEMIES%"),
    NO_PARTIES_AVAILABLE("&aThere are currently no parties available! Make one with /party create"),
    WIN_MESSAGE_SOLO("&aThe game is over, the winner is %player%!"),
    WIN_MESSAGE_MULTIPLE("&aThe game is over, the winners are %team%"),
    EDITING_KIT("&aYou are now editing your kit! Once your kit is ready, type /save to confirm"),
    RANKED_DISABLED("&cThe ranked gamemode is disabled on this server!"),
    ELO_LIST("Your ELO's:"),
    PVP_KICK_QUEUE("You have been removed from the queue."),
    STATS_LOADING("Your stats are still loading, please be patient."),
    CLICK_TO_JOIN_PARTY("Click here to accept the party invite!"),
    LOSER_DETAILS("Loser Details:"),
    WINNER_DETAILS("Winner Details:"),
    MATCH_DURATION("&aMatch Duration: %TIME%"),
    ENEMY_HP("&aEnemy HP:"),
    WINNER_DETAILS_KILLS_MULTI("&aYour team has beaten %KILLED% enemies!"),
    WINNER_DETAILS_DEATHS_MULTI("&aYour team lost %KILLED% players! RIP"),
    EDIT_KIT_LORE("Click here to edit this kit to your preferences."),
    EDIT_GUI_NAME("Kit Editor"),
    ALREADY_EDITING_KITS("You can't do this as you are already editing kits! Save to quit!"),
    CANNOT_SAVE("You can't save as you are not editing a kit!"),
    CLICK_TO_LEAVE_QUEUE("Click here to leave the queue or type /battle leave!");

    private String line;
    private static String cl = "%%__USER__%%";

    private LangConfig(String line){
        this.line = line;
    }

    public static void load(){
        cl = cl + "";
        File f = new File(Bukkit.getPluginManager().getPlugin("TrainingPvP").getDataFolder(), "Language.yml");
        if(!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);

        for(LangConfig lc : LangConfig.values()){
            if(!config.contains(lc.name())) {
                config.set(lc.name(), lc.getString());
            }
        }
        for(LangConfig lc : LangConfig.values()){
            String line = config.getString(lc.name());
            lc.setString(ChatColor.translateAlternateColorCodes('&', line));
        }
        try {
            config.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getString(){
        return ChatColor.translateAlternateColorCodes('&', line);
    }

    public String getString(Player p) {
        String l = line;
        if(l.contains("%player%")) {
            l = l.replace("%player%", p.getName());
        }
        return ChatColor.translateAlternateColorCodes('&', l);

    }

    public void setString(String line){
        this.line = line;
    }

}
