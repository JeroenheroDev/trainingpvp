package nl.jeroenhero.TrainingPvP.Objects;

import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.BookMeta;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeroen on 14:31 17-8-2016.
 */
public class Team {

    private HashMap<Player, Boolean> alive = new HashMap<Player, Boolean>();

    public ArrayList<Player> players;

    public Team(ArrayList<Player> players) {
        this.players = players;
        for(Player player : players) {
            alive.put(player, true);
            PlayerData pld = PlayerData.getPlayerData(player);
            pld.setCurrentTeam(this);
        }
    }

    public boolean isAlive(Player player) {
        return alive.get(player);
    }

    public void setAlive(Player player, boolean alive) {
        this.alive.put(player, alive);
    }

    public int getAlivePlayersSize() {
        int alive = 0;
        for(Player player: this.alive.keySet()) {
            if(this.alive.get(player)) {
                alive ++;
            }
        }
        return alive;
    }

    public ArrayList<Player> getAlivePlayers() {
        ArrayList<Player> list = new ArrayList<Player>();
        for(Player player : alive.keySet()) {
            if(isAlive(player))
                list.add(player);
        }
        return list;
    }
}
