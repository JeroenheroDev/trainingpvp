package nl.jeroenhero.TrainingPvP.Arenas;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Jeroen on 11:27 15-7-2016.
 */
public class ArenaManager {

    public static ArrayList<Arena> arenas = new ArrayList<Arena>();
    public static Location serverSpawn;
    public static Location kitEditLocation;

    public ArenaManager () {
        ServiceLocator.setArenaManager(this);
        setup();
    }

    private void setup() {
        loadArenas();
        loadSpawn();
    }

    public void loadSpawn() {
        File f = new File(TrainingPvP.getInstance().getDataFolder(), "spawn.yml");
        if(!f.exists()) {
            try {
                f.createNewFile();
                FileConfiguration config = YamlConfiguration.loadConfiguration(f);
                config.set("spawn", "World%0.0%0.0%0.0%0.0%0.0");
                config.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);

        if(config.contains("spawn")) {
            serverSpawn = locationFromString(config.getString("spawn"));
        }
        else
            System.out.println("No server spawn has been setup!!!! You have to do this in order to play!");
        if(config.contains("kiteditlocation")) {
            kitEditLocation = locationFromString(config.getString("kiteditlocation"));
        }
    }

    public void setServerLocation(Location loc, String location) {
        File f = new File(TrainingPvP.getInstance().getDataFolder(), "spawn.yml");
        if(!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        config.set(location, locationToString(loc));
        switch(location) {
            case "spawn":
                serverSpawn = loc;
            case "kiteditlocation":
                kitEditLocation = loc;
        }
        try {
            config.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reload() {
        arenas.clear();
        loadArenas();
    }

    private void loadArenas() {
        Bukkit.getScheduler().scheduleAsyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                System.out.println("Loading arena's asynchronously!");
                File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/");
                if(!f.exists()) {
                    f.mkdirs();
                }
                for(File arenaFile : f.listFiles()) {
                    if(arenaFile.getName().endsWith(".yml")) {
                        loadArena(arenaFile);
                    }
                }
            }
        }, 50L);
    }

    public void loadArena(File file) {
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        if(config == null) {
            return;
        }
        Arena a = new Arena();
        try {
            a.setName(config.getString("name"));
            a.setCreator(config.getString("creator"));
            a.setCreateData(config.getString("createData"));
            a.setCreateLocation(locationFromString(config.getString("createLocation")));
            if(config.contains("modes")) {
                a.setModes(config.getStringList("modes"));
            }
            if(config.contains("spawns")) {
                List<String> spawnStrings = config.getStringList("spawns");
                List<Location> spawnList = new ArrayList<Location>();
                for(String s : spawnStrings) {
                    spawnList.add(locationFromString(s));
                }
                for(Location loc : spawnList) {
                    a.addSpawn(loc);
                }
                arenas.add(a);
            }
            else {
                System.out.println("Didn't load arena: " + a.getName() + " due to it not having any spawns setup!");
            }

        }
        catch (Exception e) {
            System.out.println("Something went wrong while trying to load " + file.getName());
            e.printStackTrace();
        }
    }

    public void createArenaConfig(String name, Player creator) {
        File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/");
        if(!f.exists()) {
            f.mkdirs();
        }
        f = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/", name + ".yml");
        if(f.exists()) {
            creator.sendMessage(Config.prefix + ChatColor.RED + "An arena with this name already exists!");
            return;
        }
        for(Arena a : ArenaManager.arenas) {
            if(a.getName() == name) {
                creator.sendMessage(Config.prefix + ChatColor.RED + "An arena with this name already exists!");
                return;
            }
        }
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        config.options().copyDefaults(true);
        config.addDefault("name", name);
        config.addDefault("creator", creator.getName());
        config.addDefault("createLocation", this.locationToString(creator.getLocation()));
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        config.addDefault("createDate", dateFormat.format(date));
        try {
            config.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
        creator.sendMessage(Config.prefix + "Succesfully created the arena " + name);
    }

    public boolean removeSpawn(String arenaName, Location spawnLocation) {
        File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/", arenaName +  ".yml");
        if(!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        if(!config.contains("spawns")) {
            return false;
        }
        List<String> spawns = config.getStringList("spawns");
        HashMap<Location, String> spawnLocs = new HashMap<Location, String>();
        for(String s : spawns) {
            spawnLocs.put(locationFromString(s), s);
        }
        boolean edit = false;
        for(Location loc : spawnLocs.keySet()) {
            if(loc.getBlockX() == spawnLocation.getBlockX() && loc.getBlockY() == spawnLocation.getBlockY() && loc.getBlockZ() == spawnLocation.getBlockZ()) {
                spawns.remove(spawnLocs.get(loc));
                edit = true;
            }
        }
        if(edit) {
            config.set("spawns", spawns);
            try {
                config.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;

    }

    public void addSpawn(String arenaName, Location loc) {
        File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/", arenaName +  ".yml");
        if(!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        List<String> spawnList = new ArrayList<String>();
        if(config.contains("spawns")) {
            spawnList = config.getStringList("spawns");
        }
        spawnList.add(locationToString(loc));
        config.set("spawns", spawnList);
        try {
            config.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removeArena(String s) {
        System.out.println("Trying to remove " + s);
        if(!s.endsWith(".yml")) {
            s = s + ".yml";
        }
        File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/", s);
        if(f.exists()) {
            f.delete();
        }
    }

    public boolean arenaExists(String name) {
        File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Arenas/", name + ".yml");
        return f.exists();
    }
    public String locationToString(Location loc) {
        return loc.getWorld().getName() + "%" + loc.getX() + "%" + loc.getY() + "%" + loc.getZ() + "%" + loc.getYaw() + "%" + loc.getPitch();
    }

    public Location locationFromString(String s) {
        String[] strings = s.split("%");
        return new Location(Bukkit.getWorld(strings[0]), Double.parseDouble(strings[1]), Double.parseDouble(strings[2]),
                Double.parseDouble(strings[3]), Float.parseFloat(strings[4]), Float.parseFloat(strings[5]));
    }
}
