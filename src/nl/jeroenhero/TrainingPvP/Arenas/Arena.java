package nl.jeroenhero.TrainingPvP.Arenas;

import nl.jeroenhero.TrainingPvP.Objects.Match;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jeroen on 11:27 15-7-2016.
 */
public class Arena {

    private List<Location> spawns = new ArrayList<Location>();
    private String name;
    private List<String> modes = new ArrayList<String>();
    private String creator;
    private String createData;
    private Location createLocation;
    private ArrayList<Match> currentMatches = new ArrayList<Match>();
    private boolean tempDisabled = false;
    private boolean inUse = false;


    public List<Location> getSpawns() {
        return spawns;
    }

    public void setSpawns(List<Location> spawns) {
        this.spawns = spawns;
    }

    public void addSpawn(Location loc) {
        spawns.add(loc);
    }

    public boolean removeSpawn(Location loc) {
        if(spawns.contains(loc)) {
            spawns.remove(loc);
            return true;
        }
        else {
            for(Location l : spawns) {
                if(l.getWorld() == loc.getWorld() && l.getBlockX() == loc.getBlockX()) {
                    if(l.getBlockZ() == loc.getBlockZ() && l.getBlockY() == loc.getBlockY()) {
                        spawns.remove(l);
                        return true;
                    }
                }
            }
        }
        return  false;
    }

    public boolean isInUse (){
        return inUse;
    }

    public void setInUse(boolean b) {
        inUse = b;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getModes() {
        return modes;
    }

    public void setModes(List<String> modes) {
        this.modes = modes;
    }

    public String getCreateData() {
        return createData;
    }

    public void setCreateData(String createData) {
        this.createData = createData;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public ArrayList<Match> getCurrentMatches() {
        return currentMatches;
    }

    public void setCurrentMatches(ArrayList<Match> currentMatches) {
        this.currentMatches = currentMatches;
    }

    public void addMatch(Match m) {
        currentMatches.add(m);
    }

    public void removeMatch(Match m) {
        currentMatches.remove(m);
    }

    public boolean isTempDisabled() {
        return tempDisabled;
    }

    public void setTempDisabled(boolean tempDisabled) {
        this.tempDisabled = tempDisabled;
    }

    public Location getCreateLocation() {
        return createLocation;
    }

    public void setCreateLocation(Location createLocation) {
        this.createLocation = createLocation;
    }
}
