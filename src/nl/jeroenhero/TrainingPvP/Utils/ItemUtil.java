package nl.jeroenhero.TrainingPvP.Utils;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jeroen on 15:11 19-10-2016.
 */
public class ItemUtil {

    public static List<String> addChatColorToLore(List<String> lore) {
        List<String> newLore = new ArrayList<String>();
        for(String s : lore) {
            newLore.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        return newLore;
    }

    public static void setName(String name, ItemStack item) {
        ItemMeta itm = item.getItemMeta();
        itm.setDisplayName(name);
        item.setItemMeta(itm);
    }



}
