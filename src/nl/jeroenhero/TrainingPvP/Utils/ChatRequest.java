package nl.jeroenhero.TrainingPvP.Utils;

import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;

import java.util.ArrayList;

/**
 * Created by Jeroen on 21:09 19-7-2016.
 */
public class ChatRequest implements Listener{

    public static ArrayList<ChatRequest> requests = new ArrayList<ChatRequest>();

    private Player player;
    private ChatAction action;
    private boolean async;

    public ChatRequest(Player player, ChatAction action, boolean async) {
        requests.add(this);
        this.player = player;
        this.action = action;
        this.async = async;
        Bukkit.getPluginManager().registerEvents(this, TrainingPvP.getInstance());
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        if(!async) { return; }
        if(e.getPlayer() == player) {
            e.setCancelled(true);
            requests.remove(this);
            HandlerList.unregisterAll(this);
            action.onChat(e.getMessage());
        }
    }

    @EventHandler
    public void onPlayerChat(PlayerChatEvent e) {
        if(async) { return; }
        if(e.getPlayer() == player) {
            e.setCancelled(true);
            requests.remove(this);
            HandlerList.unregisterAll(this);
            action.onChat(e.getMessage());
        }
    }


    public interface ChatAction {
        public void onChat(String message);
    }
}
