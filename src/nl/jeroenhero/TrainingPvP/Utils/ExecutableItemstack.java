package nl.jeroenhero.TrainingPvP.Utils;

import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Created by Jeroen on 21:31 18-7-2016.
 */
public class ExecutableItemstack implements Listener{

    public static ArrayList<ExecutableItemstack> stacks = new ArrayList<ExecutableItemstack>();

    private Player player;
    private ClickAction action;
    private ItemStack item;
    private String invName;

    public ExecutableItemstack(ItemStack it, Player player, ClickAction onClick, String invName) {
        this.player = player;
        this.action = onClick;
        this.item = it;
        this.invName = invName;
        stacks.add(this);
        Bukkit.getPluginManager().registerEvents(this, TrainingPvP.getInstance());
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        ArrayList<ExecutableItemstack> remove = new ArrayList<ExecutableItemstack>();
        if(player.getName() == e.getWhoClicked().getName() && invName == e.getInventory().getName()) {
            if(e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
                return;
            }
            if(e.getCurrentItem().getType() != item.getType()) {
                return;
            }
            if(e.getCurrentItem().getItemMeta().getDisplayName() != item.getItemMeta().getDisplayName()) {
                return;
            }
            if(e.getCurrentItem().getAmount() != item.getAmount()) {
                return;
            }
            action.onClick(e);
            e.setCancelled(true);
            stacks.remove(this);
            HandlerList.unregisterAll(this);
        }
        for(ExecutableItemstack stack : remove) {
            stacks.remove(stack);
        }
    }

    public static interface ClickAction {
        public void onClick(InventoryClickEvent event);
        
    }
}
