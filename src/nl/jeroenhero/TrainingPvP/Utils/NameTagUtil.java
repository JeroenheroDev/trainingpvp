package nl.jeroenhero.TrainingPvP.Utils;

import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;

/**
 * Created by Jeroen on 16:15 29-8-2016.
 */
public class NameTagUtil {

    public static ChatColor getTeamColor(int number) {
        switch(number) {
            case 0: return ChatColor.RED;
            case 1: return ChatColor.YELLOW;
            case 2: return ChatColor.BLUE;
            case 3: return ChatColor.LIGHT_PURPLE;
            case 4: return ChatColor.AQUA;
            case 5: return ChatColor.GOLD;
            default: return ChatColor.RED;
        }
    }

    @EventHandler
    public void onNameTag(AsyncPlayerReceiveNameTagEvent event) {
        Player player = event.getPlayer();
        PlayerData pd = PlayerData.getPlayerData(player);
    }

}
