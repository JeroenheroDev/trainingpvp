package nl.jeroenhero.TrainingPvP.Utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jeroen on 13:48 12-11-2016.
 */
public class InventorySaver {

    private HashMap<UUID, SavedInventory> invMap = new HashMap<UUID, SavedInventory>();

    public void saveInventory(Player player) {
        SavedInventory invSave = new SavedInventory(player);
        invMap.put(player.getUniqueId(), invSave);
    }

    public void setOldInventory(Player player) {
        if(hasInventorySaved(player)){
            SavedInventory inv = invMap.get(player.getUniqueId());
            inv.returnItems();
            invMap.remove(player.getUniqueId());
        }
    }

    public boolean hasInventorySaved(Player player) {
        return invMap.containsKey(player.getUniqueId());
    }


    public class SavedInventory {

        private Player player;
        private HashMap<Integer, ItemStack> itemMap;
        private HashMap<Integer, Integer> amountMap;

        public SavedInventory(Player player) {
            this.player = player;
            itemMap = new HashMap<Integer, ItemStack>();
            amountMap = new HashMap<Integer, Integer>();
            for(int i = 0; i < 41; i ++) {
                if((player.getInventory().getSize() + 3) < i)
                    continue;
                if(player.getInventory().getItem(i) != null) {
                    itemMap.put(i, player.getInventory().getItem(i));
                    amountMap.put(i, player.getInventory().getItem(i).getAmount());
                }
            }

        }

        public void returnItems() {
            player.getInventory().clear();
            PlayerInventory inv = player.getInventory();
            for(Integer i : itemMap.keySet()) {
                ItemStack it = itemMap.get(i);
                if(it != null) {
                    it.setAmount(amountMap.get(i));
                    player.getInventory().setItem(i, it);
                }
            }
        }
    }
}
