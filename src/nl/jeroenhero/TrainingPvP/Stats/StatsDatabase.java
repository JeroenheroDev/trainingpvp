package nl.jeroenhero.TrainingPvP.Stats;

import nl.jeroenhero.TrainingPvP.Enums.RankedType;
import nl.jeroenhero.TrainingPvP.Kits.Kit;
import nl.jeroenhero.TrainingPvP.Kits.KitManager;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Jeroen on 16:47 22-11-2016.
 */
public class StatsDatabase {

    private Connection connection;

    public StatsDatabase() {
        if(!Config.mySQL) {
            System.out.println("NOT Using MySQL! It is really recommended to enable MySQL!");
            return;
        }
        Bukkit.getScheduler().runTaskAsynchronously(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                createTableIfNotExists();
            }
        });

    }

    public void saveStatsAsync(final PlayerStats stats){
        if(!Config.mySQL) {
            return;
        }
        Bukkit.getScheduler().runTaskAsynchronously(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                saveStats(stats);
                saveElo(stats);
            }
        });
    }

    public void loadStatsAsync(final Player player) {
        if(!Config.mySQL) {
            return;
        }
        Bukkit.getScheduler().runTaskAsynchronously(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                loadStats(player);
            }
        });
    }

    public void saveStats(PlayerStats stats) {
        MySQL sql = new MySQL(Config.mySQLIP, Config.mySQLPort, Config.mySQLDatabase, Config.mySQLUserName, Config.mySQLPassWord);
        if(stats == null) {
            System.out.println("Failed to save playerstats, stats are null!");
            return;
        }
        try {
            sql.openConnection();
            sql.updateSQL("UPDATE TrainingPvPStats SET playerName=\"" + stats.name + "\", kills=" + stats.kills + ", " +
                    "deaths=" + stats.deaths +  ", gamesPlayed=" + stats.gamesPlayed + ", gamesWon=" + stats.gamesWon + " WHERE uuid=\"" + stats.uuid + "\";");
            sql.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void saveElo(PlayerStats stats) {
        MySQL sql = new MySQL(Config.mySQLIP, Config.mySQLPort, Config.mySQLDatabase, Config.mySQLUserName, Config.mySQLPassWord);
        if(stats == null) {
            System.out.println("Failed to save elo, stats are null!");
            return;
        }
        try {
            sql.openConnection();
            HashMap<String, Integer> eloMap = stats.getEloMap();
            for(String s : eloMap.keySet()) {
                sql.updateSQL("UPDATE TrainingPvPElo SET " + s + "=" + eloMap.get(s) + " WHERE uuid=\"" + stats.uuid + "\";");
            }
            sql.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createTableIfNotExists() {
        if(!Config.mySQL) {
            return;
        }
        MySQL sql = new MySQL(Config.mySQLIP, Config.mySQLPort, Config.mySQLDatabase, Config.mySQLUserName, Config.mySQLPassWord);
        try {
            sql.openConnection();
            sql.updateSQL("CREATE TABLE IF NOT EXISTS TrainingPvPStats (playerName VARCHAR(20) NOT NULL PRIMARY KEY, uuid VARCHAR(36) NOT NULL, kills INT(10), deaths INT(10), gamesPlayed INT(10), gamesWon INT(10), reg_date TIMESTAMP)");
            sql.updateSQL("CREATE TABLE IF NOT EXISTS TrainingPvPElo (playerName VARCHAR(20) NOT NULL PRIMARY KEY, uuid VARCHAR(36) NOT NULL)");
            for(Kit k : KitManager.kits) {
                if(k.getRankType() != RankedType.RANKED)
                    continue;
                try {
                    sql.updateSQL("ALTER TABLE TrainingPvPElo ADD " + k.getName() + " INT(5);");
                }
                catch (Exception e){}
            }
            sql.closeConnection();
        } catch (SQLException e) {
            System.out.println();
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void loadStats(Player player) {
        if(!Config.mySQL) {
            return;
        }
        PlayerData pd = PlayerData.getPlayerData(player);
        if(pd.getStats() == null) {
            pd.setStats(new PlayerStats(player));
        }
        PlayerStats stats = pd.getStats();
        MySQL sql = new MySQL(Config.mySQLIP, Config.mySQLPort, Config.mySQLDatabase, Config.mySQLUserName, Config.mySQLPassWord);
        try {
            sql.openConnection();
            ResultSet set = sql.querySQL("SELECT * FROM TrainingPvPStats WHERE uuid = \"" + player.getUniqueId().toString() + "\"");
            if(set.next()) {
                stats.name = player.getName();
                stats.uuid = player.getUniqueId();
                stats.kills = set.getInt("kills");
                stats.deaths = set.getInt("deaths");
                stats.gamesPlayed = set.getInt("gamesPlayed");
                stats.gamesWon = set.getInt("gamesWon");
            }
            else {
                System.out.println("Saving new stats for player " + player.getName());
                stats.gamesPlayed = 0;
                stats.gamesWon = 0;
                stats.deaths = 0;
                stats.kills = 0;
                stats.name = player.getName();
                stats.uuid = player.getUniqueId();
                sql.updateSQL("INSERT IGNORE INTO TrainingPvPStats (`playerName`, `uuid`, `kills`, `deaths`, `gamesPlayed`, `gamesWon`) VALUES (\"" + stats.name + "\", \"" + stats.uuid.toString() + "\"," + stats.kills + ", " + stats.deaths + ", " + stats.gamesPlayed + ", " + stats.gamesWon + ");");
            }
            sql.closeConnection();
        } catch (SQLException e) {
            System.out.println("Failed to load MySQL stats for player: " + player.getName() + ". Stacktrace:");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Failed to load MySQL stats for player: " + player.getName() + ". Stacktrace:");
            e.printStackTrace();
        }
        loadElo(player);
    }

    public void loadElo(Player player) {
        if(!Config.mySQL) {
            return;
        }
        PlayerData pd = PlayerData.getPlayerData(player);
        if(pd.getStats() == null) {
            pd.setStats(new PlayerStats(player));
        }
        PlayerStats stats = pd.getStats();
        MySQL sql = new MySQL(Config.mySQLIP, Config.mySQLPort, Config.mySQLDatabase, Config.mySQLUserName, Config.mySQLPassWord);
        try {
            sql.openConnection();
            ResultSet set = sql.querySQL("SELECT * FROM TrainingPvPElo WHERE uuid = \"" + player.getUniqueId().toString() + "\"");
            if(set.next()) {
                for(Kit k : KitManager.kits) {
                    if(k.getRankType() == RankedType.UNRANKED)
                        continue;
                    try {
                        if(set.getInt(k.getName()) == 0)
                            stats.setElo(k.getName(), Config.defaultElo);
                        else
                            stats.setElo(k.getName(), set.getInt(k.getName()));
                    }
                    catch(Exception e) {
                        stats.setElo(k.getName(), Config.defaultElo);
                    }
                }
            }
            else {
                System.out.println("Saving new ELO for player " + player.getName());
                for(Kit k : KitManager.kits) {
                    if(k.getRankType() == RankedType.UNRANKED)
                        continue;
                    if(stats.getElo(k.getName()) == -1) {
                        stats.setElo(k.getName(), Config.defaultElo);
                    }
                }
                sql.updateSQL("INSERT IGNORE INTO TrainingPvPElo (`playerName`, `uuid`) VALUES (\"" + stats.name + "\", \"" + stats.uuid.toString() + "\");");
            }
            sql.closeConnection();
        } catch (SQLException e) {
            System.out.println("Failed to load MySQL ELO for player: " + player.getName() + ". Stacktrace:");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Failed to load MySQL ELO for player: " + player.getName() + ". Stacktrace:");
            e.printStackTrace();
        }
    }

}
