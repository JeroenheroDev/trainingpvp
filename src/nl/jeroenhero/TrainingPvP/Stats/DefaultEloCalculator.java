package nl.jeroenhero.TrainingPvP.Stats;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created by Jeroen on 17:22 27-12-2016.
 */
public class DefaultEloCalculator extends EloCalculator {

    @Override
    public void calculateELO(Player winner, Player loser, int winnerElo, int loserElo, String eloKitName) {
        double expectedScoreWinner = 1.0/(1.0 + (Math.pow(10.0, ((loserElo - winnerElo)/400.0))));
        double expectedScoreLoser = 1.0 - expectedScoreWinner;

        double newEloA = winnerElo + (Config.eloKFactor * (1 - expectedScoreWinner));
        double newEloB = loserElo + (Config.eloKFactor * (0 - expectedScoreLoser));

        int newEloAI = (int)Math.round(newEloA);
        int newEloBI = (int)Math.round(newEloB);

        setNewElo(winner, newEloAI, eloKitName);
        setNewElo(loser, newEloBI, eloKitName);

        //.broadcastMessage("Set ELO For " + winner.getName() + " to " + newEloAI);
        //Bukkit.broadcastMessage("Set ELO For " + loser.getName() + " to " + newEloBI);

        String eloUpdate = LangConfig.ELO_UPDATE.getString();
        eloUpdate = eloUpdate.replace("%WINNER%", winner.getDisplayName());
        eloUpdate = eloUpdate.replace("%LOSER%", loser.getDisplayName());
        eloUpdate = eloUpdate.replace("%WINNER_ELO%", newEloAI + "");
        eloUpdate = eloUpdate.replace("%LOSER_ELO%", newEloBI + "");
        eloUpdate = eloUpdate.replace("%WINNER_CHANGE%", "+" + (newEloAI - winnerElo) + "");
        eloUpdate = eloUpdate.replace("%LOSER_CHANGE%", "-" + (loserElo - newEloBI) + "");

        winner.sendMessage(eloUpdate);
        loser.sendMessage(eloUpdate);

    }

}
