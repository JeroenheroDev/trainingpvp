package nl.jeroenhero.TrainingPvP.Stats;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Jeroen on 17:05 27-12-2016.
 */
public abstract class EloCalculator {

    public static EloCalculator mainCalculator;

    public abstract void calculateELO(Player winner, Player loser, int winnerElo, int loserElo, String eloKitName);

    public static void calculateElo(Player winner, Player loser, String eloKitName) {
        mainCalculator.calculateELO(winner, loser, getElo(winner, eloKitName), getElo(loser, eloKitName), eloKitName);
    }

    private static int getElo(Player player, String eloKitName) {
        PlayerData pd = PlayerData.getPlayerData(player);
        PlayerStats ps = pd.getStats();
        if(!ps.getEloMap().containsKey(eloKitName)) {
            return Config.defaultElo;
        }
        if(ps.getEloMap().get(eloKitName) != -1) {
            return ps.getEloMap().get(eloKitName);
        }
        else {
            return Config.defaultElo;
        }
    }

    public void setNewElo(Player player, int newElo, String eloKitName) {
        PlayerData pd = PlayerData.getPlayerData(player);
        PlayerStats ps = pd.getStats();
        ps.setElo(eloKitName, newElo);
    }
}
