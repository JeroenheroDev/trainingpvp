package nl.jeroenhero.TrainingPvP.Stats;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Jeroen on 16:47 22-11-2016.
 */
public class PlayerStats {

    public int kills,deaths,gamesPlayed,gamesWon,rankedPlayedDaily;
    public UUID uuid;
    public String name;

    private HashMap<String, Integer> eloMap = new HashMap<String, Integer>();


    public PlayerStats(Player player) {
        uuid = player.getUniqueId();
        name = player.getName();
        if(Config.mySQL)
            ServiceLocator.getStatsDatabase().loadStatsAsync(player);
    }

    public void setElo(String eloKit, Integer elo) {
        eloMap.put(eloKit, elo);
    }

    public int getElo(String eloKit) {
        if(eloMap.containsKey(eloKit)) {
            return eloMap.get(eloKit);
        }
        else {
            return -1;
        }

    }

    public HashMap<String, Integer> getEloMap () {
        return eloMap;
    }

    public void save() {
        ServiceLocator.getStatsDatabase().saveStats(this);
        ServiceLocator.getStatsDatabase().saveElo(this);
    }

}
