package nl.jeroenhero.TrainingPvP.Signs;

import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by jeroen on 14-04-17.
 */
public abstract class TrainingPvPSign {

    public static ArrayList<TrainingPvPSign> signList = new ArrayList<>();

    public static ArrayList<CommandSign> commandSigns = new ArrayList<>();

    public static ArrayList<ArenaSign> arenaSignList = new ArrayList<>();

    public static void setup() {
    }

    public abstract void onClick(Player player);

    public abstract String getID();

    public abstract void update();

}
