package nl.jeroenhero.TrainingPvP.Signs;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

/**
 * Created by Jeroen on 16:55 4-1-2017.
 */
public class SignListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if(event.getClickedBlock() != null) {
            Block block = event.getClickedBlock();
            if(block.getType() == Material.SIGN || block.getType() == Material.WALL_SIGN || block.getType() == Material.SIGN_POST) {
                if(block.getState() instanceof Sign) {
                    Sign s = (Sign) block.getState();

                }
            }
        }
    }

    @EventHandler
    public void onBuild(BlockPlaceEvent event) {
        Player placer = event.getPlayer();
        if(!event.isCancelled()) {
            if(event.getBlock().getType() == Material.SIGN || event.getBlock().getType() == Material.WALL_SIGN || event.getBlock().getType() == Material.SIGN_POST) {
                Sign s = (Sign) event.getBlock().getState();
                if(s.getLine(0) != null) {
                    if(s.getLine(0).equalsIgnoreCase("TrainingPvP")) {
                        Permission signPerm = new Permission("trainingpvp.signs", PermissionDefault.OP);
                        if(!placer.hasPermission(signPerm)) {
                            placer.sendMessage(Config.prefix + LangConfig.NO_PERMISSION.getString());
                            event.setCancelled(true);
                            return;
                        }
                        if(s.getLine(1) != null) {
                            String ID = s.getLine(1);
                        }
                    }

                }
            }
        }
    }
}
