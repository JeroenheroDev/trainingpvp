package nl.jeroenhero.TrainingPvP.Kits;

import nl.jeroenhero.TrainingPvP.Arenas.ArenaManager;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Jeroen on 16:15 1-11-2016.
 */
public class EditableKitManager {

    private HashMap<Player, ConfigKit> editingKits = new HashMap<Player, ConfigKit>();

    public void setIsEditingKits(Player player, boolean edit, ConfigKit kit) {
        if (edit)
            editingKits.put(player, kit);
        else
            editingKits.remove(player);
    }

    public Set<Player> getEditingKits() {
        return editingKits.keySet();
    }

    public EditableKitManager() {

    }

    public void editKit(final Player player, final ConfigKit kit) {
        if(ServiceLocator.getArenaManager().kitEditLocation == null) {
            player.sendMessage(Config.prefix + "For editing kits the server owner has to setup the edit location first! (/Arena)");
            return;
        }
        if(!kit.isEditable()) {
            player.sendMessage(Config.prefix + "This kit can't be edited.");
            return;
        }
        Location editKitLocation = ServiceLocator.getArenaManager().kitEditLocation;
        player.closeInventory();
        player.teleport(editKitLocation);
        setIsEditingKits(player, true, kit);
        for (Player players : getEditingKits()) {
            if(players != player) {
                player.hidePlayer(players);
                players.hidePlayer(player);
            }
        }
        ServiceLocator.getInventorySaver().saveInventory(player);
        Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                player.getInventory().clear();

            }
        }, 10L);
        Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                kit.giveItems(player);
                player.updateInventory();
            }
        }, 20L);
        player.sendMessage(Config.prefix + LangConfig.EDITING_KIT.getString());
    }

    public boolean stopAndSave(final Player player) {
        if(!editingKits.containsKey(player)) {
            return false;
        }

        ConfigKit kit = editingKits.get(player);
        if(!kit.isEditable())
            return false;
        editingKits.remove(player);
        File dir = new File(TrainingPvP.getInstance().getDataFolder() + "/PlayerData");
        if(!dir.exists()) {
            dir.mkdirs();
        }
        File f = new File(TrainingPvP.getInstance().getDataFolder() + "/PlayerData/", player.getUniqueId().toString() + ".yml");
        if(!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        if(config.contains("kits." + kit.getName())) {
            config.set("kits." + kit.getName(), null);
        }
        player.closeInventory();

        for(int i = 0; i < 41; i ++) {
            if((player.getInventory().getSize() + 3) < i) {
                continue;
            }
            if(player.getInventory().getItem(i) == null)
                continue;
            Bukkit.broadcastMessage("Slot " + i + " : " + player.getInventory().getItem(i).getType());
        }
        for(int i = 0; i < 41; i ++) {
            if((player.getInventory().getSize() + 3) < i)
                continue;

            if(player.getInventory().getItem(i) != null) {
                ItemStack it = player.getInventory().getItem(i);
                if(it.getType() != Material.AIR) {
                    config.set("kits." + kit.getName() + ".items." + i + "", it);
                    Bukkit.broadcastMessage("Set " + i + "to " + it);
                }
            }
        }
        try {
            Bukkit.broadcastMessage("Attempting to save now");
            config.save(f);
            player.sendMessage(Config.prefix + "Kit edited and saved!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.closeInventory();
        Bukkit.getScheduler().scheduleSyncDelayedTask(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                if(1 == 1)
                    return;
                player.closeInventory();
                player.teleport(ServiceLocator.getArenaManager().serverSpawn);
                player.getInventory().clear();
                player.getInventory().setHelmet(new ItemStack(Material.AIR));
                player.getInventory().setChestplate(new ItemStack(Material.AIR));
                player.getInventory().setLeggings(new ItemStack(Material.AIR));
                player.getInventory().setBoots(new ItemStack(Material.AIR));
                ServiceLocator.getInventorySaver().setOldInventory(player);
            }
        }, 10L);

        return true;
    }

    public void cancel(Player player) {
        if(!editingKits.containsKey(player)) {
            return;
        }
        player.closeInventory();
        player.getInventory().clear();
        ServiceLocator.getInventorySaver().setOldInventory(player);
        player.teleport(ArenaManager.serverSpawn);
    }

    public void giveEditedKit(Player player, ConfigKit kit) {
        if(!kit.isEditable()) {
            kit.giveItems(player);
            return;
        }
        File f = new File( TrainingPvP.getInstance().getDataFolder() + "/PlayerData/", player.getUniqueId().toString() + ".yml");
        if(!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        if(config.contains("kits." + kit.getName())) {
            for(int i = 0; i < 41; i ++) {
                if(player.getInventory().getSize() + 3 < i)
                    continue;
                if(config.contains("kits." + kit.getName() + ".items." + i)) {
                    ItemStack it = config.getItemStack("kits." + kit.getName() + ".items." + i);
                    player.getInventory().setItem(i, it);
                }
            }
        }
        else {
            kit.giveItems(player);
        }
    }

}
