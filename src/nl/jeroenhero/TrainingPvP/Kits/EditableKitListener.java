package nl.jeroenhero.TrainingPvP.Kits;

import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Jeroen on 16:13 1-11-2016.
 */
public class EditableKitListener implements Listener {

    public EditableKitListener() {

    }

    private boolean isEditingKit(Player player) {
        return ServiceLocator.getEditableKitManager().getEditingKits().contains(player);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if(isEditingKit(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if(isEditingKit(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event) {
        if(isEditingKit(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if(event.getDamager() instanceof Player) {
            if(isEditingKit((Player)event.getDamager()))
                event.setCancelled(true);
        }
        if(event.getEntity() instanceof Player) {
            if(!(event.getDamager() instanceof Player))
                return;
            if(isEditingKit((Player)event.getDamager()))
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDead(PlayerDeathEvent event) {
        if(isEditingKit(event.getEntity()))
            event.getDrops().clear();
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        if(isEditingKit(event.getPlayer()))
            ServiceLocator.getEditableKitManager().cancel(event.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        if(isEditingKit(event.getPlayer())) {
            PlayerData pd = PlayerData.getPlayerData(event.getPlayer());
            pd.setLeft(true);
        }
        event.getPlayer().getInventory().clear();
        event.getPlayer().getInventory().setBoots(new ItemStack(Material.AIR));
        event.getPlayer().getInventory().setChestplate(new ItemStack(Material.AIR));
        event.getPlayer().getInventory().setLeggings(new ItemStack(Material.AIR));
        event.getPlayer().getInventory().setHelmet(new ItemStack(Material.AIR));


    }

    @EventHandler
    public void onConsume(PlayerItemConsumeEvent event) {
        if(isEditingKit(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onShoot(EntityShootBowEvent event) {
        if(event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if(isEditingKit(player)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onThrow(PlayerInteractEvent e){
        if(e.getPlayer() == null)
            return;
        if(e.getItem() == null)
            return;
        if(e.getItem().getType() == null)
            return;
        ItemStack it = e.getItem();
        boolean wrong = false;
        switch(it.getType().toString()) {
            case "POTION":
                wrong = true;
            case "ENDER_PEARL":
                wrong = true;
            case "SPLASH_POTION":
                wrong = true;
            case "LINGERING_POTION":
                wrong = true;

        }
        if(wrong) {
            if (isEditingKit(e.getPlayer()))
                e.setCancelled(true);
        }
    }

}
