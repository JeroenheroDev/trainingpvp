package nl.jeroenhero.TrainingPvP.Kits;

import nl.jeroenhero.TrainingPvP.Enums.RankedType;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import nl.jeroenhero.TrainingPvP.Utils.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Jeroen on 11:08 19-8-2016.
 */
public class KitManager {

    public static ArrayList<Kit> kits = new ArrayList<Kit>();

    public void loadKits() {
        loadConfigKits();
    }

    public void loadConfigKits() {
        Bukkit.getScheduler().runTaskAsynchronously(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                File kitsFolder = new File(TrainingPvP.getInstance().getDataFolder() + "/Kits/");
                if(!kitsFolder.exists()) {
                    kitsFolder.mkdirs();
                }
                for(File f : kitsFolder.listFiles()) {
                    if(f.getName().endsWith(".yml")) {
                        loadConfigKit(f);
                    }
                }
            }
        });

    }

    public void createConfigKit(final String name, final Player player) {
        Bukkit.getScheduler().runTaskAsynchronously(TrainingPvP.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                File folder = new File(TrainingPvP.getInstance().getDataFolder() + "/Kits/");
                if(!folder.exists()) {
                    folder.mkdirs();
                }
                File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Kits/", name + ".yml");
                if(f.exists()) {
                    player.sendMessage(Config.prefix + "This kit already exists! Try another name.");
                    return;
                }
                else {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                    FileConfiguration config = YamlConfiguration.loadConfiguration(f);
                    config.set("creator", player.getName());
                    config.set("name", name);
                    config.set("gui-item.name", "&a" + name);
                    config.set("gui-item.type", "DIAMOND_SWORD");
                    List<String> lore = new ArrayList<String>() { {
                        add("The amazing " + name + " kit! Try it now!");
                    }};
                    config.set("gui-item.lore", lore);
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    config.addDefault("createDate", dateFormat.format(date));
                    config.set("ranked", false);
                    config.set("enabled", true);
                    config.set("permission-required", false);
                    config.set("permission", "trainingpvp.kit." + name);
                    config.set("editable", true);
                    for(int i = 0; i < 41; i ++) {
                        if((player.getInventory().getSize() + 3) < i)
                            continue;
                        if(player.getInventory().getItem(i) != null) {
                            ItemStack it = player.getInventory().getItem(i);
                            if(it.getType() != Material.AIR) {
                                config.set("items." + i + "", it);
                            }
                        }
                    }
                    try {
                        config.save(f);
                        player.sendMessage(Config.prefix + "Kit created!");
                        loadConfigKit(f);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void loadConfigKit(File f){
        if(!f.exists()) {
            return;
        }
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(f));
            if(bufferedReader.readLine() == null) {
                return;
            }
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if(bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Loading kit " + f.getName());
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        ConfigKit configKit = new ConfigKit();
        for(int i = 0; i < 41; i ++) {
            if(config.contains("items." + i + "")) {
                configKit.setItem(i, config.getItemStack("items." + i + ""));
            }
        }
        configKit.setName(config.getString("name"));
        String s = config.getString("ranked");
        if(s.equalsIgnoreCase("true"))
            configKit.setRankedType(RankedType.RANKED);
        else if(s.equalsIgnoreCase("false"))
            configKit.setRankedType(RankedType.UNRANKED);
        else if(s.equalsIgnoreCase("both"))
            configKit.setRankedType(RankedType.BOTH);
        else {
            configKit.setRankedType(RankedType.UNRANKED);
        }
        configKit.setPermission(config.getString("permission"));
        configKit.setPermissionRequired(config.getBoolean("permission-required"));
        configKit.setEditable(config.getBoolean("editable"));
        ItemStack it = new ItemStack(Material.valueOf(config.getString("gui-item.type").toUpperCase()));
        ItemMeta itm = it.getItemMeta();
        itm.setDisplayName(ChatColor.translateAlternateColorCodes('&', config.getString("gui-item.name")));
        List<String> lore = config.getStringList("gui-item.lore");
        itm.setLore(ItemUtil.addChatColorToLore(lore));
        it.setItemMeta(itm);
        configKit.setGUIItem(it);
        kits.add(configKit);
    }

    public void removeKit(String s) {
        removeKit(s, null);
    }

    public void removeKit(String s, Player player) {
        File f = new File(TrainingPvP.getInstance().getDataFolder() + "/Kits/");
        if(!f.exists()) {
            f.mkdirs();
            player.sendMessage(Config.prefix + "This kit doesn't exist! Create one first.");
        }
        else {
            f = new File(TrainingPvP.getInstance().getDataFolder() + "/Kits/", s + ".yml");
            if(!f.exists()) {
                player.sendMessage(Config.prefix + "This kit doesn't exist! Create one first.");
                return;
            }
            f.delete();
            player.sendMessage(Config.prefix + "Kit has been removed. Please reload kits for it to take effect.");

        }
    }



}
