package nl.jeroenhero.TrainingPvP.Kits;

import nl.jeroenhero.TrainingPvP.Enums.RankedType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;

/**
 * Created by Jeroen on 10:20 19-8-2016.
 */
public class ConfigKit extends Kit {

    private HashMap<Integer, ItemStack> itemMap;
    private RankedType rankedType;
    private String name;
    private boolean permissionRequired;
    private String permission;
    private ItemStack it;
    private boolean editable;

    public ConfigKit() {
        itemMap = new HashMap<Integer, ItemStack>();
    }

    @Override
    public void giveItems(Player player) {
        player.getInventory().clear();
        PlayerInventory inv = player.getInventory();
        for(int i = 0; i < 41; i ++) {
            if(itemMap.containsKey(i))
                inv.setItem(i, itemMap.get(i).clone());
        }
        player.updateInventory();
    }

    @Override
    public RankedType getRankType() {
        return rankedType;
    }


    public void setItem(int itemSlot, ItemStack item) {
        itemMap.put(itemSlot, item);
    }

    public void setRankedType(RankedType type) {
        this.rankedType = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean requiresPermission() {
        return permissionRequired;
    }

    @Override
    public ItemStack getGuiItem() {
        return it.clone();
    }

    public void setPermissionRequired(boolean permissionRequired) {
        this.permissionRequired = permissionRequired;
    }

    @Override
    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public void setGUIItem(ItemStack it) {
        this.it = it;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
