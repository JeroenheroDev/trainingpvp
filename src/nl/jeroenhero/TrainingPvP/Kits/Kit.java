package nl.jeroenhero.TrainingPvP.Kits;

import nl.jeroenhero.TrainingPvP.Enums.RankedType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Jeroen on 09:50 19-8-2016.
 */
public abstract class Kit {

    public abstract void giveItems(Player player);

    public abstract RankedType getRankType();

    public boolean requiresPermission() {
        return true;
    }

    public abstract String getName();

    public String getPermission() {
        return "trainingpvp.kit." + getName();
    }

    public abstract ItemStack getGuiItem();

    public boolean isOwnItems() {
        return false;
    }

    public void onGameStart() {}

}
