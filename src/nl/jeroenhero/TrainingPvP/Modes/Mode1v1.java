package nl.jeroenhero.TrainingPvP.Modes;

import nl.jeroenhero.TrainingPvP.Objects.BattleMode;

/**
 * Created by Jeroen on 13:30 18-7-2016.
 */
public class Mode1v1 extends BattleMode {


    @Override
    public int getTeamSize() {
        return 1;
    }

    @Override
    public boolean isPartyRequired() {
        return false;
    }

    @Override
    public boolean isPartySplit() {
        return false;
    }

    @Override
    public String getName() {
        return "1V1";
    }


}
