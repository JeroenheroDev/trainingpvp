package nl.jeroenhero.TrainingPvP.Modes;

import nl.jeroenhero.TrainingPvP.Objects.BattleMode;

/**
 * Created by Jeroen on 13:32 18-7-2016.
 */
public class ModePVP extends BattleMode{

    @Override
    public int getTeamSize() {
        return -1;
    }

    @Override
    public boolean isPartyRequired() {
        return true;
    }

    @Override
    public boolean isPartySplit() {
        return false;
    }

    @Override
    public String getName() {
        return "Party VS Party";
    }
}
