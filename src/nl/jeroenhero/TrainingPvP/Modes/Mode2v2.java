package nl.jeroenhero.TrainingPvP.Modes;

import nl.jeroenhero.TrainingPvP.Objects.BattleMode;

/**
 * Created by Jeroen on 13:32 18-7-2016.
 */
public class Mode2v2 extends BattleMode {
    @Override
    public int getTeamSize() {
        return 2;
    }

    @Override
    public boolean isPartyRequired() {
        return true;
    }

    @Override
    public boolean isPartySplit() {
        return false;
    }

    @Override
    public String getName() {
        return "2V2";
    }
}
