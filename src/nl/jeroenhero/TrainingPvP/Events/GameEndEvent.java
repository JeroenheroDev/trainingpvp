package nl.jeroenhero.TrainingPvP.Events;

import nl.jeroenhero.TrainingPvP.Arenas.Arena;
import nl.jeroenhero.TrainingPvP.Kits.Kit;
import nl.jeroenhero.TrainingPvP.Objects.Match;
import nl.jeroenhero.TrainingPvP.Objects.Team;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * by Arraying @ 22/11/2016
 * This class is potentially incomplete.
 * Please add to it.
 */
public class GameEndEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private Team winners;
    private Match match;
    private Arena arena;
    private Kit kit;
    private boolean ranked;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public GameEndEvent(Team winners, Match match, Arena arena, Kit kit, boolean ranked) {
        this.winners = winners;
        this.match = match;
        this.arena = arena;
        this.kit = kit;
        this.ranked = ranked;
    }

    public Team getWinningTeam() {
        return winners;
    }

    public Match getMatch() {
        return match;
    }

    public Kit getKit() {
        return kit;
    }

    public boolean isRanked() {
        return ranked;
    }

    public Arena getArena() {
        return arena;
    }

}
