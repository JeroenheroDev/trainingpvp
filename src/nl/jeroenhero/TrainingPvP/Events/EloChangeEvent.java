package nl.jeroenhero.TrainingPvP.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * by Arraying @ 22/11/2016
 * Useful for people who want to work with ELO.
 * This class is potentially incomplete.
 * Please add to it.
 */
public class EloChangeEvent extends Event{

    private static final HandlerList handlers= new HandlerList();

    private Player player;
    private int eloBefore;
    private int eloAfter;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public EloChangeEvent(Player player, int eloBefore, int eloAfter) {
        this.player = player;
        this.eloBefore = eloBefore;
        this.eloAfter = eloAfter;
    }

    public Player getPlayer() {
        return player;
    }

    public int getEloBefore() {
        return eloBefore;
    }

    public int getEloAfter() {
        return eloAfter;
    }

}
