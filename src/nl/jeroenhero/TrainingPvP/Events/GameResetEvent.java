package nl.jeroenhero.TrainingPvP.Events;

import nl.jeroenhero.TrainingPvP.Arenas.Arena;
import nl.jeroenhero.TrainingPvP.Objects.Match;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Jeroen on 10:10 16-12-2016.
 *
 * This Event is called when the game has ended and the map / player data and information is being reset.
 * This is when all players have left the map.
 * Can be used for resetting the map, removing item drops etc.
 */
public class GameResetEvent extends Event{

    private Match match;
    private Arena arena;
    private ArrayList<UUID> playerUUIDs;


    public GameResetEvent (Match match, Arena arena, ArrayList<UUID> players) {
        this.match = match;
        this.arena = arena;
        this.playerUUIDs = players;
    }

    public Match getMatch() {
        return match;
    }
    public Arena getArena() {
        return arena;
    }

    public ArrayList<UUID> getPlayerUUIDs() {
        return playerUUIDs;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private static final HandlerList handlers = new HandlerList();

}
