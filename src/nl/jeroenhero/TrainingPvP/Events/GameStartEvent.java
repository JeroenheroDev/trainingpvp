package nl.jeroenhero.TrainingPvP.Events;

import nl.jeroenhero.TrainingPvP.Arenas.Arena;
import nl.jeroenhero.TrainingPvP.Kits.Kit;
import nl.jeroenhero.TrainingPvP.Objects.Match;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Jeroen on 18:52 20-9-2016.
 */
public class GameStartEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private Match match;
    private Arena arena;
    private Kit kit;
    private boolean ranked;

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public GameStartEvent(Match m, Arena a, Kit kit, boolean ranked) {
        match = m;
        arena = a;
        this.kit = kit;
        this.ranked = ranked;
    }

    public Match getMatch() {
        return match;
    }

    public Kit getKit() {
        return kit;
    }

    public boolean isRanked() {
        return ranked;
    }

    public Arena getArena() {
        return arena;
    }

}
