package nl.jeroenhero.TrainingPvP.Enums;

/**
 * Created by Jeroen on 16:29 26-10-2016.
 */
public enum MatchStage {

    PREPARE,INGAME,AFTERGAME,ENDED;
}
