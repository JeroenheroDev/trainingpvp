package nl.jeroenhero.TrainingPvP.Enums;

/**
 * Created by Jeroen on 09:55 19-8-2016.
 */
public enum RankedType {

    RANKED, UNRANKED, BOTH;
}
