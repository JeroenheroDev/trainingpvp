package nl.jeroenhero.TrainingPvP;

import nl.jeroenhero.TrainingPvP.Arenas.ArenaManager;
import nl.jeroenhero.TrainingPvP.Commands.*;
import nl.jeroenhero.TrainingPvP.Hooks.CombatLogHook;
import nl.jeroenhero.TrainingPvP.Inventories.MatchSelector;
import nl.jeroenhero.TrainingPvP.Kits.EditableKitListener;
import nl.jeroenhero.TrainingPvP.Kits.KitManager;
import nl.jeroenhero.TrainingPvP.Listeners.EntityListener;
import nl.jeroenhero.TrainingPvP.Listeners.FactionsListener;
import nl.jeroenhero.TrainingPvP.Listeners.PlayerListener;
import nl.jeroenhero.TrainingPvP.Listeners.PlayerPacketListener;
import nl.jeroenhero.TrainingPvP.Modes.Mode1v1;
import nl.jeroenhero.TrainingPvP.Modes.Mode2v2;
import nl.jeroenhero.TrainingPvP.Modes.ModePVP;
import nl.jeroenhero.TrainingPvP.Objects.BattleMode;
import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.Objects.LangConfig;
import nl.jeroenhero.TrainingPvP.Objects.PlayerData;
import nl.jeroenhero.TrainingPvP.SpawnItems.ItemListener;
import nl.jeroenhero.TrainingPvP.Stats.DefaultEloCalculator;
import nl.jeroenhero.TrainingPvP.Stats.EloCalculator;
import nl.jeroenhero.TrainingPvP.Stats.PlayerStats;
import nl.jeroenhero.TrainingPvP.Utils.ExecutableItemstack;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;

/**
 * Created by Jeroen on 20:20 14-7-2016.
 */
public class TrainingPvP extends JavaPlugin{

    private static TrainingPvP instance;

    public static TrainingPvP getInstance() {
        return instance;

    }

    @Override
    public void onDisable() {
        if(!Config.mySQL)
            return;
        for(PlayerData pd : PlayerData.dataMap.values()) {
            try {
                pd.getStats().save();
            }
            catch (Exception e) {}
        }
    }

    @Override
    public void onEnable() {
        instance = this;
        setup();

    }

    private void registerEvents() {
        //Bukkit.getPluginManager().registerEvents(new PlayerPacketListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        new EntityListener();
        Bukkit.getPluginManager().registerEvents(new EditableKitListener(), this);
        Bukkit.getPluginManager().registerEvents(new ItemListener(), this);
    }

    private void registerCommands() {
        new CommandBattle();
        new CommandArena();
        new CommandRanked();
        new CommandUnranked();
        new CommandTrainingPvP();
        new CommandElo();
        try {
            final Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
            commandMap.register(Config.kitCommand, new CommandKit(Config.kitCommand));
            if(Config.partyEnabled)
                commandMap.register(Config.partyCommand, new CommandParty(Config.partyCommand));
            if(Config.editKits) {
                commandMap.register(Config.editKitCommand, new CommandEdit(Config.editKitCommand));
                commandMap.register(Config.saveCommand, new CommandSave(Config.saveCommand));
            }
            if(Config.duelEnabled)
                commandMap.register(Config.duelCommand, new CommandDuel(Config.duelCommand));
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    private void setup() {
        Config.load();
        LangConfig.load();
        registerCommands();
        registerEvents();
        BattleMode.registerMode(new Mode1v1());
        BattleMode.registerMode(new Mode2v2());
        BattleMode.registerMode(new ModePVP());
        KitManager manager = new KitManager();
        manager.loadKits();
        ServiceLocator.setKitManager(manager);
        ServiceLocator.getStatsDatabase();
        EloCalculator.mainCalculator = new DefaultEloCalculator();
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new BukkitRunnable() {
            @Override
            public void run() {
                delayedSetup();
            }
        }, 20L);
    }

    private void delayedSetup() {
        new ArenaManager();
        new FactionsListener();
        new CombatLogHook();
    }


}
