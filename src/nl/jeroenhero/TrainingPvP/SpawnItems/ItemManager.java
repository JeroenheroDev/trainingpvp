package nl.jeroenhero.TrainingPvP.SpawnItems;

import nl.jeroenhero.TrainingPvP.Objects.Config;
import nl.jeroenhero.TrainingPvP.ServiceLocator;
import nl.jeroenhero.TrainingPvP.TrainingPvP;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Jeroen on 11:53 23-2-2017.
 */
public class ItemManager {

    public static boolean enabled = true;

    public static ArrayList<SpawnItem> spawnItems = new ArrayList<SpawnItem>();

    public ItemManager() {
        ServiceLocator.setItemManager(this);
        enabled = Config.spawnItemsEnabled;
        if(enabled)
            loadItemsFromConfig();
    }

    public static void giveSpawnItems(Player player) {
        player.getInventory().clear();
        for(SpawnItem s : spawnItems) {
            player.getInventory().addItem(s);
        }
    }

    public void loadItemsFromConfig() {
        File f = new File(TrainingPvP.getInstance().getDataFolder(), "spawnItems.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        Set<String> items = config.getKeys(false);
        if(items.size() == 0) {
            System.out.println("No items could be loaded. Disabling this feature.");
            enabled = false;
        }
        for(String s : items) {
            loadItem(f, config, s);
        }

    }

    public void loadItem(File f, FileConfiguration config, String key) {
        if(!config.contains(key)) {
            return;
        }
        String material = config.getString(key + ".material");
        int data = config.getInt(key + ".data");
        int amount = config.getInt(key + ".amount");
        String displayName = config.getString(key + ".displayName");
        List<String> lore = new ArrayList<String>();
        if(config.contains(key + ".lore")) {
            for(String s : config.getConfigurationSection(key + ".lore").getKeys(false)) {
                lore.add(config.getString(key + ".lore." + s));
            }
        }
        ItemStack it = new ItemStack(Material.valueOf(material), data);
        it.setAmount(amount);
        ItemMeta itm = it.getItemMeta();
        itm.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
        List<String> orLore = itm.getLore();
        for(String s : lore){
            orLore.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        itm.setLore(lore);
        it.setItemMeta(itm);
        SpawnItem s = new SpawnItem(it);
        s.setHasCommand(config.getBoolean(key + ".hasCommand"));
        String command = config.getString(key + ".command");
        boolean consoleCommand = config.getBoolean(key + ".consoleCommand");
        s.setCommand(command);
        s.setConsoleCommand(consoleCommand);
        spawnItems.add(s);
    }



}
