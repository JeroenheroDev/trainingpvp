package nl.jeroenhero.TrainingPvP.SpawnItems;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Jeroen on 11:54 23-2-2017.
 */
public class ItemListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(event.getPlayer() == null) {return;}
        if(event.getItem() == null) { return; }
        Player player = event.getPlayer();
        ItemStack it = event.getItem();
        if(it instanceof SpawnItem) {
            SpawnItem spawnItem = (SpawnItem) it;
            spawnItem.onInteract(player);
        }
    }
}
