package nl.jeroenhero.TrainingPvP.SpawnItems;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Jeroen on 11:54 23-2-2017.
 */
public class SpawnItem extends ItemStack {

    private ItemStack it;

    private boolean hasCommand = false;
    private String command = "";
    private boolean consoleCommand = false;

    public SpawnItem(ItemStack it) {
        this.it = it;
        super.setType(it.getType());
        super.setAmount(it.getAmount());
        super.setData(it.getData());
        super.setItemMeta(it.getItemMeta());
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public boolean isConsoleCommand() {
        return consoleCommand;
    }

    public void setConsoleCommand(boolean consoleCommand) {
        this.consoleCommand = consoleCommand;
    }

    public boolean isHasCommand() {
        return hasCommand;
    }

    public void setHasCommand(boolean hasCommand) {
        this.hasCommand = hasCommand;
    }

    public void onInteract(Player player) {
        if(!hasCommand)
            return;
        if(consoleCommand) {
            Bukkit.getConsoleSender().sendMessage(command.replace("%PLAYER%", player.getName()));
        }
        else {
            player.performCommand(command.replace("%PLAYER%", player.getName()));
        }
    }


}
